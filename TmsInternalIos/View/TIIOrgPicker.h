//
//  TIIOrgPicker.h
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-29.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface TIIOrgPicker : NSObject

@property (nonatomic) NSInteger selectedIndex;
@property (nonatomic, weak) UIButton * btnTarget;


- (id)initWithTargetButton:(UIButton *)btnTarget;
- (id)initWithTargetButton:(UIButton *)btnTarget withkeyArray:(NSArray *)keyArray valueArray:(NSArray *)valueArray;
- (void)show;
- (void)dismiss;
- (NSString *)selectedOrgCode;
- (void)setSelectedOrgCode:(NSString *)orgCode;

@end
