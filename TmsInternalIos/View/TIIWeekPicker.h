//
//  TIIWeekPicker.h
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-29.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TIIWeekPicker;

@protocol TIIWeekPickerDelegate <NSObject>

@optional
- (void)tiiWeekPickerDidSelected:(TIIWeekPicker *)weekPicker;

@end
@interface TIIWeekPicker : NSObject

@property (nonatomic) int selectedYear;
@property (nonatomic) int selectedWeek;
@property (nonatomic, weak) id<TIIWeekPickerDelegate> delegate;

- (NSString *)selectedYearWeekWithW;
- (void)setSelectedYearWeek:(NSString *)yearWeek;

- (void)show;
- (void)dismiss;

@end
