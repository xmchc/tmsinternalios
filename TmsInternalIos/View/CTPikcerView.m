//
//  CTPikcerView.m
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-29.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTPikcerView.h"
#import <QuartzCore/QuartzCore.h>

@interface CTPikcerView () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) UIWindow * window;     // 全局弹窗
@property (nonatomic, strong) UIView * contentView;  // 内容
@property (nonatomic, strong) UIPickerView * picker;
@property (nonatomic, strong) UILabel * dateInterval; //日期区间
@property (nonatomic) NSInteger componentNum;

- (void)onButtonClicked:(UIButton *)btn;

@end

@implementation CTPikcerView

- (id)initWithComponentNum:(NSInteger)componentNum keyArray:(NSArray *)keyArray valueArray:(NSArray *)valueArray delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle confirmButtonTitle:(NSString *)confirmButtonTitle
{
    if (self = [super initWithFrame:[[UIScreen mainScreen] bounds]]) {
        
        self.componentNum = componentNum;
        self.keyArray = keyArray;
        self.valueArray = valueArray;
        self.picker = [[UIPickerView alloc] init];
        self.picker.frame = CGRectMake(0, 0, 240, self.picker.frame.size.height);
        self.picker.dataSource = self;
        self.picker.delegate = self;
        self.picker.showsSelectionIndicator = YES;
        [self.picker reloadAllComponents];
        
        self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.picker.frame.size.width + 20, self.picker.frame.size.height + 10 + 36 + 10 + 10)];
        self.contentView.layer.cornerRadius = 5.0;
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.contentView.layer.borderColor = [[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1] CGColor];
        self.contentView.layer.borderWidth = 0.5;
        self.contentView.layer.shadowRadius = 1;
        self.contentView.layer.shadowOffset = CGSizeMake(1, 1);
        
        self.picker.center = CGPointMake(10 + self.picker.frame.size.width / 2, 10 + self.picker.frame.size.height / 2);
        [self.contentView addSubview:self.picker];
        
        int y = self.picker.frame.size.height + 20;
        int x = 10;
        int tag = 0;
        
        if (self.dateInterval == nil) {
            self.dateInterval = [[UILabel alloc] initWithFrame:CGRectMake(0, y - 25, self.contentView.frame.size.width, 20)];
            self.dateInterval.font = [UIFont systemFontOfSize:15.0f];
            self.dateInterval.textAlignment = NSTextAlignmentCenter;
            [self.contentView addSubview:self.dateInterval];
        }
        
        if (cancelButtonTitle != nil) {
            UIButton * btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
            btnCancel.frame = CGRectMake(x, y, (self.contentView.frame.size.width - 30) / 2, 36);
            btnCancel.backgroundColor = [UIColor clearColor];
            [btnCancel setBackgroundImage:[[UIImage imageNamed:@"TIIBtnRed"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 9, 14, 9)] forState:UIControlStateNormal];
            [btnCancel setBackgroundImage:[[UIImage imageNamed:@"TIIBtnRedPressed"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 9, 14, 9)] forState:UIControlStateHighlighted];
            btnCancel.titleLabel.font = [UIFont systemFontOfSize:14.0f];
            [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnCancel setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
            [btnCancel setTitle:cancelButtonTitle forState:UIControlStateNormal];
            [btnCancel setTitle:cancelButtonTitle forState:UIControlStateHighlighted];
            [btnCancel addTarget:self action:@selector(onButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            btnCancel.tag = tag;
            tag++;
            [self.contentView addSubview:btnCancel];
            x += btnCancel.frame.size.width + 10;
        }
        
        if (confirmButtonTitle != nil) {
            UIButton * btnConfirm = [UIButton buttonWithType:UIButtonTypeCustom];
            btnConfirm.frame = CGRectMake(x, y, (self.contentView.frame.size.width - 30) / 2, 36);
            btnConfirm.backgroundColor = [UIColor clearColor];
            [btnConfirm setBackgroundImage:[[UIImage imageNamed:@"TIIBtnRed"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 9, 14, 9)] forState:UIControlStateNormal];
            [btnConfirm setBackgroundImage:[[UIImage imageNamed:@"TIIBtnRedPressed"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 9, 14, 9)] forState:UIControlStateHighlighted];
            btnConfirm.titleLabel.font = [UIFont systemFontOfSize:14.0f];
            [btnConfirm setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnConfirm setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
            [btnConfirm setTitle:confirmButtonTitle forState:UIControlStateNormal];
            [btnConfirm setTitle:confirmButtonTitle forState:UIControlStateHighlighted];
            [btnConfirm addTarget:self action:@selector(onButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            btnConfirm.tag = tag;
            tag++;
            [self.contentView addSubview:btnConfirm];
            x += btnConfirm.frame.size.width + 10;
        }
        
        
        self.contentView.center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
        
        [self addSubview:self.contentView];
        
        self.delegate = delegate;
        
    }
    return self;
}

- (void)show
{
    if (self.window == nil) {
        self.window = [[UIWindow alloc] initWithFrame:self.bounds];
        self.window.windowLevel = UIWindowLevelAlert;
        self.window.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
        self.window.opaque = NO;
        self.window.alpha = 0.0;
        [self.window addSubview:self];
    }
    
    [self.layer removeAllAnimations];
    [UIView animateWithDuration:0.2 animations:^{
        self.window.alpha = 1.0;
    } completion:^(BOOL finished){
    }];
    
    CAKeyframeAnimation *popAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    popAnimation.duration = 0.4;
    popAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.01f, 0.01f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.02f, 1.02f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.97f, 0.97f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DIdentity]];
    popAnimation.keyTimes = @[@0.2f, @0.5f, @0.75f, @1.0f];
    popAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.layer addAnimation:popAnimation forKey:nil];
    [self.window makeKeyAndVisible];
    if ([self.delegate respondsToSelector:@selector(ctPickerView:didReceiveDateInterval:)]) {
        [self.delegate ctPickerView:self didReceiveDateInterval:self.dateInterval];
    }
}

- (void)dismiss
{
    [self.layer removeAllAnimations];
    [UIView animateWithDuration:0.2 animations:^{
        self.window.alpha = 0.0;
        [self.window resignKeyWindow];
    } completion:^(BOOL finished){
        self.window = nil;
    }];
    
    
}

- (void)onButtonClicked:(UIButton *)btn
{
    [self dismiss];
    if ([self.delegate respondsToSelector:@selector(ctPickerView:didClickedButtonOnIndex:)]) {
        [self.delegate ctPickerView:self didClickedButtonOnIndex:btn.tag];
    }
}

- (UIPickerView *)pickerView
{
    return self.picker;
}

#pragma mark -
#pragma mark UIPickerView Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return self.componentNum;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.componentNum < 2) {
        return self.valueArray.count;
    } else {
        return [[self.valueArray objectAtIndex:component] count];
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 36;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    UILabel * value = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width / self.componentNum, 36)];
    value.backgroundColor = [UIColor clearColor];
    value.font = [UIFont systemFontOfSize:14.0];
    value.textColor = [UIColor blackColor];
    value.textAlignment = NSTextAlignmentCenter;
    if (self.componentNum < 2) {
        value.text = [self.valueArray objectAtIndex:row];
    } else {
        value.text = [[self.valueArray objectAtIndex:component] objectAtIndex:row];
    }
    return value;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if ([self.delegate respondsToSelector:@selector(ctPickerView:didReceiveDateInterval:)]) {
        [self.delegate ctPickerView:self didReceiveDateInterval:self.dateInterval];
    }
}



@end
