//
//  TIIAlertView.h
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-28.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTAlertView;

@protocol CTAlertViewDelegate <NSObject>

@optional

- (void)ctAlertView:(CTAlertView *)alertView didClickedButtonOnIndex:(NSInteger)index;

@end

@interface CTAlertView : UIView

@property (nonatomic, weak) id<CTAlertViewDelegate> delegate;
@property (nonatomic) NSInteger tag;

- (id)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle confirmButtonTitle:(NSString *)confirmButtonTitle;

- (void)show;

- (void)dismiss;

@end
