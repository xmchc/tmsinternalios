//
//  TIIAlertView.m
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-28.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTAlertView.h"
#import <QuartzCore/QuartzCore.h>

@interface CTAlertView ()

@property (nonatomic, strong) UIWindow * window;     // 全局弹窗
@property (nonatomic, strong) UILabel * titleLabel;  // 标题
@property (nonatomic, strong) UIView * contentView;  // 内容

- (void)onButtonClicked:(UIButton *)btn;

@end

@implementation CTAlertView

- (id)init
{
    NSAssert(1 != 1, @"CTAlertView init not recommend method");
    return nil;
}

- (id)initWithFrame:(CGRect)frame
{
    NSAssert(1 != 1, @"CTAlertView initWithFrame not recommend method");
    return nil;
}

- (id)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle confirmButtonTitle:(NSString *)confirmButtonTitle
{
    if (self = [super initWithFrame:[UIScreen mainScreen].bounds]) {
        
        self.delegate = delegate;
        
        UIView * container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 240, 0)];
        container.backgroundColor = [UIColor clearColor];
        
        UIImageView * titleBg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, container.frame.size.width, 40)];
        titleBg.image = [[UIImage imageNamed:@"AlertTitleBg"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 14, 15, 14)];
        titleBg.contentMode = UIViewContentModeScaleToFill;
        [container addSubview:titleBg];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, container.frame.size.width - 20, titleBg.frame.size.height - 10)];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        self.titleLabel.textColor = [UIColor whiteColor];
        self.titleLabel.numberOfLines = 1;
        self.titleLabel.text = title;
        [container addSubview:self.titleLabel];
        
        self.contentView = [[UIView alloc] initWithFrame:CGRectMake(0, titleBg.frame.size.height, container.frame.size.width, 0)];
        
        UIImageView * contentBg = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"AlertContentBg"] resizableImageWithCapInsets:UIEdgeInsetsMake(13, 14, 13, 14)]];
        contentBg.contentMode = UIViewContentModeScaleToFill;
        [self.contentView addSubview:contentBg];
        
        UIScrollView * scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, 0)];
        scroll.backgroundColor = [UIColor clearColor];
        
        UILabel * contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, self.contentView.frame.size.width - 20, 0)];
        contentLabel.font = [UIFont systemFontOfSize:14.0f];
        contentLabel.numberOfLines = 0;
        contentLabel.textColor = [UIColor blackColor];
        contentLabel.backgroundColor = [UIColor clearColor];
        CGSize size = [message sizeWithFont:contentLabel.font constrainedToSize:CGSizeMake(contentLabel.frame.size.width, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
        size = CGSizeMake(size.width, size.height + 10);
        if (size.height > 200) {
            scroll.frame = CGRectMake(0, 0, self.contentView.frame.size.width, 200);
        } else if (size.height < 75){
            scroll.frame = CGRectMake(0, 0, self.contentView.frame.size.width, 75);
        } else {
            scroll.frame = CGRectMake(0, 0, self.contentView.frame.size.width, size.height);
        }
        contentLabel.frame = CGRectMake(contentLabel.frame.origin.x, contentLabel.frame.origin.y, contentLabel.frame.size.width, size.height);
        contentLabel.text = message;
        [scroll addSubview:contentLabel];
        scroll.contentSize = size;
        [self.contentView addSubview:scroll];
        
        int y = scroll.frame.size.height + 10;
        int x = 10;
        int tag = 0;
        int width = self.contentView.frame.size.width - 20;
        if (cancelButtonTitle != nil) {
            UIButton * btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
            btnCancel.frame = CGRectMake(x, y, (self.contentView.frame.size.width - 30) / 2, 36);
            btnCancel.backgroundColor = [UIColor clearColor];
            [btnCancel setBackgroundImage:[[UIImage imageNamed:@"TIIBtnRed"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 9, 14, 9)] forState:UIControlStateNormal];
            [btnCancel setBackgroundImage:[[UIImage imageNamed:@"TIIBtnRedPressed"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 9, 14, 9)] forState:UIControlStateHighlighted];
            btnCancel.titleLabel.font = [UIFont systemFontOfSize:14.0f];
            [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnCancel setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
            [btnCancel setTitle:cancelButtonTitle forState:UIControlStateNormal];
            [btnCancel setTitle:cancelButtonTitle forState:UIControlStateHighlighted];
            [btnCancel addTarget:self action:@selector(onButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            btnCancel.tag = tag;
            tag++;
            [self.contentView addSubview:btnCancel];
            x += btnCancel.frame.size.width + 10;
            width = (self.contentView.frame.size.width - 30) / 2;
        }
        
        if (confirmButtonTitle != nil) {
            UIButton * btnConfirm = [UIButton buttonWithType:UIButtonTypeCustom];
            btnConfirm.frame = CGRectMake(x, y, width, 36);
            btnConfirm.backgroundColor = [UIColor clearColor];
            [btnConfirm setBackgroundImage:[[UIImage imageNamed:@"TIIBtnRed"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 9, 14, 9)] forState:UIControlStateNormal];
            [btnConfirm setBackgroundImage:[[UIImage imageNamed:@"TIIBtnRedPressed"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 9, 14, 9)] forState:UIControlStateHighlighted];
            btnConfirm.titleLabel.font = [UIFont systemFontOfSize:14.0f];
            [btnConfirm setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnConfirm setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
            [btnConfirm setTitle:confirmButtonTitle forState:UIControlStateNormal];
            [btnConfirm setTitle:confirmButtonTitle forState:UIControlStateHighlighted];
            [btnConfirm addTarget:self action:@selector(onButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            btnConfirm.tag = tag;
            tag++;
            [self.contentView addSubview:btnConfirm];
            x += btnConfirm.frame.size.width + 10;
        }
        
        
        contentBg.frame = CGRectMake(0, 0, container.frame.size.width, y + 36 + 10);
        self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, contentBg.frame.size.height);
        [container addSubview:self.contentView];
        
        
        container.frame = CGRectMake(0, 0, container.frame.size.width, self.contentView.frame.origin.y + self.contentView.frame.size.height);
        container.center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
        [self addSubview:container];
        
    }
    return self;
}

- (void)show
{
    if (self.window == nil) {
        self.window = [[UIWindow alloc] initWithFrame:self.bounds];
        self.window.windowLevel = UIWindowLevelAlert;
        self.window.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
        self.window.opaque = NO;
        self.window.alpha = 0.0;
        [self.window addSubview:self];
    }
    
    [self.layer removeAllAnimations];
    [UIView animateWithDuration:0.2 animations:^{
        self.window.alpha = 1.0;
    } completion:^(BOOL finished){
    }];
    
    CAKeyframeAnimation *popAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    popAnimation.duration = 0.4;
    popAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.01f, 0.01f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.02f, 1.02f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.97f, 0.97f, 1.0f)],
                            [NSValue valueWithCATransform3D:CATransform3DIdentity]];
    popAnimation.keyTimes = @[@0.2f, @0.5f, @0.75f, @1.0f];
    popAnimation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                     [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.layer addAnimation:popAnimation forKey:nil];
    [self.window makeKeyAndVisible];
}

- (void)dismiss
{
    [self.layer removeAllAnimations];
    [UIView animateWithDuration:0.2 animations:^{
        self.window.alpha = 0.0;
        [self.window resignKeyWindow];
    } completion:^(BOOL finished){
        self.window = nil;
    }];
    
    
}

- (void)onButtonClicked:(UIButton *)btn
{
    [self dismiss];
    if ([self.delegate respondsToSelector:@selector(ctAlertView:didClickedButtonOnIndex:)]) {
        [self.delegate ctAlertView:self didClickedButtonOnIndex:btn.tag];
    }
}

@end
