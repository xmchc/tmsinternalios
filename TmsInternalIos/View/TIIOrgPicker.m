//
//  TIIOrgPicker.m
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-29.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIIOrgPicker.h"
#import "CTPikcerView.h"

@interface TIIOrgPicker ()<CTPikcerViewDelegate>

@property (nonatomic, strong) CTPikcerView * orgPicker;

@end

@implementation TIIOrgPicker

- (id)initWithTargetButton:(UIButton *)btnTarget
{
    if (self = [super init]) {
        self.btnTarget = btnTarget;
        self.orgPicker = [[CTPikcerView alloc] initWithComponentNum:1 keyArray:[NSArray arrayWithObjects:ORG_CODE_FZ, ORG_CODE_XM, nil] valueArray:[NSArray arrayWithObjects:ORG_NAME_FZ, ORG_NAME_XM, nil] delegate:self cancelButtonTitle:@"取消" confirmButtonTitle:@"确定"];
        _selectedIndex = -1;
    }
    return self;
}

- (id)initWithTargetButton:(UIButton *)btnTarget withkeyArray:(NSArray *)keyArray valueArray:(NSArray *)valueArray
{
    if (self = [super init]) {
        self.btnTarget = btnTarget;
        self.orgPicker = [[CTPikcerView alloc] initWithComponentNum:1
                                                           keyArray:keyArray valueArray:valueArray delegate:self cancelButtonTitle:@"取消" confirmButtonTitle:@"确定"];
        _selectedIndex = -1;
    }
    return self;
}

- (void)setSelectedIndex:(NSInteger)selectedIndex
{
    [self.orgPicker.pickerView selectRow:selectedIndex inComponent:0 animated:NO];
    [self ctPickerView:self.orgPicker didClickedButtonOnIndex:selectedIndex];
}

- (void)show
{
    [self.orgPicker show];
}

- (void)dismiss
{
    [self.orgPicker dismiss];
}

- (NSString *)selectedOrgCode
{
    if (self.selectedIndex == -1) {
        return nil;
    }
    return [self.orgPicker.keyArray objectAtIndex:self.selectedIndex];
}

- (void)setSelectedOrgCode:(NSString *)orgCode
{
    int index = -1;
    for (int i = 0; i < self.orgPicker.keyArray.count; i++) {
        if ([orgCode hasPrefix:[self.orgPicker.keyArray objectAtIndex:i]]) {
            index = i;
            break;
        }
    }
    if (index != -1) {
        [self.orgPicker.pickerView selectRow:index inComponent:0 animated:NO];
        [self ctPickerView:self.orgPicker didClickedButtonOnIndex:1];
    }
}

#pragma mark -
#pragma mark CTPickerView Delegate
- (void)ctPickerView:(CTPikcerView *)pickerView didClickedButtonOnIndex:(NSInteger)index
{
//    if ([self.delegate respondsToSelector:@selector(orgPick:didClickButtonIndex:)]) {
//        [self.delegate orgPick:self didClickButtonIndex:self.selectedIndex];
//    }
//    if (index == 0) {
//        [self.orgPicker.pickerView selectRow:self.selectedIndex inComponent:0 animated:NO];
//    } else {
        _selectedIndex = [self.orgPicker.pickerView selectedRowInComponent:0];
        if (self.btnTarget != nil) {
            [self.btnTarget setTitle:[self.orgPicker.valueArray objectAtIndex:self.selectedIndex] forState:UIControlStateNormal];
            [self.btnTarget setTitle:[self.orgPicker.valueArray objectAtIndex:self.selectedIndex] forState:UIControlStateHighlighted];
            
//        }
    }
}

@end
