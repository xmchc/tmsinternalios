//
//  CTDatePicker.h
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-29.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTDatePicker;

@protocol CTDatePickerDelegate <NSObject>

@optional

- (void)ctDatePicker:(CTDatePicker *)pickerView didClickedButtonOnIndex:(NSInteger)index;

@end

@interface CTDatePicker : UIView

@property (nonatomic, weak) id<CTDatePickerDelegate> delegate;

- (id)initWithDateFormat:(NSString *)dateFormat delegate:(id<CTDatePickerDelegate>)delegate;
- (void)show;
- (void)dismiss;
- (NSString *)selectedDateWithFormat;
- (void)setCurrentDate:(NSString *)currentDate;

@end
