//
//  TIIViewController.m
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-28.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIIViewController.h"
#import "CTNavigationController.h"
#import "TIIMainController.h"
#import "TIIWebService.h"
#import "TIILoginController.h"
#import "CTAlertView.h"
#import "TIIVersion.h"
#import "CTSoapConnection.h"
#import "TIIUnreadCount.h"

@interface TIIViewController () <CTAlertViewDelegate, CTSoapConnectionDelegate>

@property (nonatomic, strong) CTNavigationController * navController;
@property (nonatomic, strong) TIIVersion * version;
@property (nonatomic, strong) NSTimer * checkUnreadTimer;   // 检查未读消息计时器

- (void)checkUpdate;
- (void)checkUnreadMessage;

@end

@implementation TIIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [CTURLConnection setNeedsLog:YES];
    
    self.checkUnreadTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(checkUnreadMessage) userInfo:nil repeats:YES];
    [self checkUnreadMessage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (![[TIIWebService user] isLogin]) {
        TIILoginController * controller = [[TIILoginController alloc] initWithNibName:@"TIILoginController" bundle:nil];
        [self presentViewController:controller animated:YES completion:nil];
    } else if (self.navController == nil){
        TIIMainController * main = [[TIIMainController alloc] init];
        
        self.navController = [[CTNavigationController alloc] initWithRootViewController:main];
        [self.navController setNavigationBarBackgroundImage:@"TIINavBg" imgName64:@"TIINavBg"];
        
        self.navController.view.backgroundColor = [UIColor redColor];
        [self.view addSubview:self.navController.view];
        if ([[UIDevice currentDevice].systemVersion integerValue] < 7) {
            self.navController.view.bounds = CGRectMake(0, 20, self.navController.view.frame.size.width, self.navController.view.frame.size.height);
        } else {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        }
        [self checkUpdate];
    }
}

- (void)checkUnreadMessage
{
    if (![TIIWebService user].isLogin) {
        return;
    }
    NSString *params = [TIIWebService checkUnreadCountParams];
    CTSoapConnection * conn = [[CTSoapConnection alloc] initWithFunctionName:WSDL_FUNCNAME_CHECKUNREADCOUNT params:params delegate:self];
    [conn start];
    return;
}

- (void)checkUpdate
{
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[TIIWebService checkUpdateUrl] delegate:self];
    [conn start];
}

- (void)logOut
{
    [TIIWebService user].sessionLogin = NO;
    [self.navController.view removeFromSuperview];
    self.navController = nil;
    
    TIILoginController * controller = [[TIILoginController alloc] initWithNibName:@"TIILoginController" bundle:nil];
    [self presentViewController:controller animated:YES completion:nil];
    return;
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    TIIVersion * item = [[TIIVersion alloc] init];
    [item parseData:data complete:^(TIIVersion *version){
        self.version = version;
        NSDictionary* infoDict =[[NSBundle mainBundle] infoDictionary];
        int buildCode = [[infoDict objectForKey:@"CFBundleVersion"] intValue];
        if ([version.versionCode intValue] > buildCode) {
            CTAlertView * alert = [[CTAlertView alloc] initWithTitle:[NSString stringWithFormat:@"发现新版本 v%@", version.moduleVersion] message:[NSString stringWithFormat:@"更新内容：%@", version.moduleVersionExplain] delegate:self cancelButtonTitle:nil confirmButtonTitle:@"立即更新"];
            [alert show];
        }
    }];
}

- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    
}

#pragma mark -
#pragma mark CTAlertView Delegate
- (void)ctAlertView:(CTAlertView *)alertView didClickedButtonOnIndex:(NSInteger)index
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.version.versionAddr]];
}

#pragma mark -
#pragma mark CTSoapConnection DataSource & Delegate
// 请求结束执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFinishLoading:(NSData *)data
{
//    NSLog(@"UnReadCount:%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    TIIUnreadCount *unreadCnt = [[TIIUnreadCount alloc] init];
    [unreadCnt parseData:data complete:^(TIIUnreadCount *curCount) {
        [TIIWebService setUnreadCount:curCount];
        // 发送消息数量通知
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFCATION_MESSAGE_COUNT object:nil];
    }];
    return;
}

// 请求失败时执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFailWithError:(NSError *)error
{
    return;
}

@end
