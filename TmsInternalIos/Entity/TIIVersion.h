//
//  TIIVersion.h
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-29.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TIIVersion : NSObject

@property (nonatomic, strong) NSString * versionCode;
@property (nonatomic, strong) NSString * moduleVersion;
@property (nonatomic, strong) NSString * moduleVersionExplain;
@property (nonatomic, strong) NSString * updateDate;
@property (nonatomic, strong) NSString * versionAddr;

- (void)parseData:(NSData *)data complete:(void(^)(TIIVersion *))block;


@end
