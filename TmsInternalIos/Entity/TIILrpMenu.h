//
//  TIILrpMenu.h
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-29.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TIILrpMenu : NSObject


@property (nonatomic, strong) NSString * appId;
@property (nonatomic, strong) NSString * appName;
@property (nonatomic, strong) NSString * appType;
@property (nonatomic, strong) NSString * appUrl;
@property (nonatomic, strong) NSString * imgUrl;
@property (nonatomic, strong) NSString * appOrder;

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;

@end
