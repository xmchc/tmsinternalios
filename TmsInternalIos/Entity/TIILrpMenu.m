//
//  TIILrpMenu.m
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-29.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIILrpMenu.h"

@interface TIILrpMenu () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) TIILrpMenu * tmpMenu;
@property (nonatomic, strong) NSMutableString * tmpStr;
@property (nonatomic, strong) void(^block)(NSArray *array);
@end

@implementation TIILrpMenu

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"taAppModule"]) {
        self.tmpMenu = [[TIILrpMenu alloc] init];
    } else {
        self.tmpStr = [[NSMutableString alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    [self.tmpStr appendString:string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"appId"]) {
        self.tmpMenu.appId = self.tmpStr;
    } else if ([elementName isEqualToString:@"appName"]){
        self.tmpMenu.appName = self.tmpStr;
    } else if ([elementName isEqualToString:@"appOrder"]){
        self.tmpMenu.appOrder = self.tmpStr;
    } else if ([elementName isEqualToString:@"appType"]){
        self.tmpMenu.appType = self.tmpStr;
    } else if ([elementName isEqualToString:@"appUrl"]){
        self.tmpMenu.appUrl = self.tmpStr;
    } else if ([elementName isEqualToString:@"imgUrl"]){
        self.tmpMenu.imgUrl = self.tmpStr;
    } else if ([elementName isEqualToString:@"taAppModule"]){
        [self.array addObject:self.tmpMenu];
        self.tmpStr = nil;
        self.tmpMenu = nil;
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
