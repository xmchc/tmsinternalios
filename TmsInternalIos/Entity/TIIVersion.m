//
//  TIIVersion.m
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-29.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIIVersion.h"

@interface TIIVersion () <NSXMLParserDelegate>

@property (nonatomic, strong) void(^block)(TIIVersion * version);
@property (nonatomic, strong) NSMutableString * tmp;
@end

@implementation TIIVersion

- (id)init
{
    if (self = [super init]) {
        self.moduleVersion = @"0";
    }
    return self;
}

- (void)parseData:(NSData *)data complete:(void (^)(TIIVersion *))block
{
    self.block = block;
    self.tmp = [[NSMutableString alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    self.tmp = [[NSMutableString alloc] init];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    [self.tmp appendString:string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"versionCode"]) {
        self.versionCode = self.tmp;
    } else if ([elementName isEqualToString:@"moduleVersion"]){
        self.moduleVersion = self.tmp;
    } else if ([elementName isEqualToString:@"moduleVersionExplain"]){
        self.moduleVersionExplain = self.tmp;
    } else if ([elementName isEqualToString:@"updateDate"]){
        self.updateDate = self.tmp;
    } else if ([elementName isEqualToString:@"versionAddr"]){
        self.versionAddr = self.tmp;
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self);
}

@end
