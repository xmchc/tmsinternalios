//
//  TIITaApp.h
//  TmsInternalIos
//
//  Created by hsit on 14-8-12.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TIITaApp : NSObject

@property (nonatomic, strong) NSString        *appType;
@property (nonatomic, strong) void(^block)(NSArray *array);


- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;


@end
