//
//  TIIUnreadCount.h
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-23.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TIIUnreadCount : NSObject

@property (nonatomic) NSInteger unreadSysPublicCnt;
@property (nonatomic) NSInteger unreadChatCnt;
@property (nonatomic) NSInteger unreadToDoCnt;
@property (nonatomic) NSInteger unreadWarningCnt;
@property (nonatomic) NSInteger unreadRemindCnt;

- (void)parseData:(NSData *)data complete:(void(^)(TIIUnreadCount *unreadCount))block;

@end
