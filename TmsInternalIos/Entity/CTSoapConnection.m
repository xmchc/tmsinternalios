//
//  CTSoapConnection.m
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-22.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTSoapConnection.h"
#import "TIIWebService.h"

#define CTURL_TIME_OUT_TERMINAL 15

Class object_getClass(id object);

@interface CTSoapConnection()
@property (nonatomic, strong) NSMutableData *data;
@property (nonatomic, strong) NSURLConnection *conn;
@property (nonatomic, weak) Class originalClass;

@end

@implementation CTSoapConnection

- (void)dealloc
{
    self.delegate = nil;
    [self.conn cancel];
    self.conn = nil;
}

- (id)initWithFunctionName:(NSString *)functionName params:(NSString *)params delegate:(id<CTSoapConnectionDelegate>)delegate;
{
    self = [super init];
    if (self) {
        self.funcName = functionName;
        self.delegate = delegate;
        self.params = params;
        _originalClass = object_getClass(delegate);
    }
    return self;
}

- (void)start
{
    if (self.conn == nil) {
        
        //封装soap请求消息
        NSString *soapMessage = [NSString stringWithFormat:
                                 @"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                                 "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                                 "<soap:Body>\n"
                                 "<%@ xmlns=\"%@\">\n"
                                 "%@"
                                 "</%@>\n"
                                 "</soap:Body>\n"
                                 "</soap:Envelope>\n", self.funcName, SOAP_NAMESPACE, self.params, self.funcName
                                 ];
        //请求发送到的路径
        NSURL *url = [NSURL URLWithString:[TIIWebService soapUrl]];
//        if ([self.funcName isEqualToString:WSDL_FUNCNAME_UPDATEDEVICETOKEN]) {
//            url = [NSURL URLWithString:@"http://192.168.2.93:8080/MCP_Flex/webServices/IMsgCenterWS"];
//        }
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:CTURL_TIME_OUT_TERMINAL];
        NSString *msgLength = [NSString stringWithFormat:@"%d", [soapMessage length]];
        [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest addValue: [NSString stringWithFormat:@"%@%@", SOAP_NAMESPACE, self.funcName] forHTTPHeaderField:@"SOAPAction"];
        [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
        //请求
        NSURLConnection * conn = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self startImmediately:NO];
        self.conn = conn;
    }
    
    if (self.data == nil) {
        NSMutableData * data = [[NSMutableData alloc] init];
        self.data = data;
    }
    [self.conn start];
}

- (void)cancel
{
    self.delegate = nil;
    [self.conn cancel];
    self.conn = nil;
}

- (void)setConn:(NSURLConnection *)conn
{
    if (_conn != nil) {
        [self setDelegate:nil];
    }
    if (conn == nil) {
        [self setDelegate:nil];
    }
    _conn = conn;
}

#pragma mark -
#pragma mark NSURLConnection Delegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
//    NSLog(@"%@", [error description]);
    Class currentClass = object_getClass(_delegate);
    if (currentClass == _originalClass) {
        if (_delegate != nil && [_delegate respondsToSelector:@selector(connection:didFailWithError:)]) {
            [_delegate soapConnection:self didFailWithError:error];
        }
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse * httpURLResponse = (NSHTTPURLResponse *) response;
    if((httpURLResponse.statusCode / 100) != 2){
//        NSLog(@"网络响应异常: %@", [[httpURLResponse allHeaderFields] description]);
        Class currentClass = object_getClass(_delegate);
        if (currentClass == _originalClass) {
            if (_delegate != nil && [_delegate respondsToSelector:@selector(connection:didFailWithError:)]) {
                [_delegate soapConnection:self didFailWithError:nil];
            }
        }
        
        [self.conn cancel];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.data appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
//    NSLog(@"%@", [[NSString alloc] initWithData:self.data encoding:NSUTF8StringEncoding]);
    Class currentClass = object_getClass(_delegate);
    if (currentClass == _originalClass) {
        if (_delegate != nil && [_delegate respondsToSelector:@selector(connection:didFinishLoading:)]) {
            NSString *sData = [[NSString alloc] initWithData:self.data encoding:NSUTF8StringEncoding];
            sData = [sData stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
            sData = [sData stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
            NSRange preRange = [sData rangeOfString:@"<ns1:out>"];
            NSRange postRange = [sData rangeOfString:@"</ns1:out>"];
            NSInteger startIndex = preRange.location + preRange.length;
            NSInteger len = postRange.location - startIndex;
            NSString *tagData = [sData substringWithRange:NSMakeRange(preRange.location + preRange.length, len)];
            NSData *xData = [tagData dataUsingEncoding:NSUTF8StringEncoding];
            [_delegate soapConnection:self didFinishLoading:xData];
        }
    }
    
}

@end
