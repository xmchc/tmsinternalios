//
//  SystemMsg.h
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-28.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SystemMsg : NSObject

@property (nonatomic, strong) NSString * code;
@property (nonatomic, strong) NSString * msg;

- (void)parseData:(NSData *)data complete:(void(^)(SystemMsg * msg))block;

@end
