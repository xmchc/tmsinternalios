//
//  TIISystemPublic.m
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-21.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIICommonMsg.h"

@interface TIICommonMsg () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(NSArray *array);
@property (nonatomic, strong) NSString *tmpStr;
@property (nonatomic, strong) TIICommonMsg *curSysPublic;

@end

@implementation TIICommonMsg

- (id)init
{
    if (self = [super init]) {
        self.msgId = @"";
        self.msgTitle = @"";
        self.msgContent = @"";
        self.msgDate = @"";
        self.SYS_CODE = @"";
        self.MSG_TYPE = @"";
        self.HANDLE_TYPE = @"";
        self.SEND_DATE = @"";
        self.LAST_DATE = @"";
        self.SEND_PERSON = @"";
        self.SEND_PERSON_NAME = @"";
        self.SEND_DEPT = @"";
        self.SEND_DEPT_NAME = @"";
        self.SEND_DUTY = @"";
        self.SEND_DUTY_NAME = @"";
        self.MSG_URL = @"";
        self.MSG_URL_PAD = @"";
        self.MSG_URL_PHONE = @"";
        self.expand = YES;
        self.msgFrom = @"";
    }
    return self;
}

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"MESSAGE"]) {
        self.curSysPublic = [[TIICommonMsg alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    self.tmpStr = [string isEqualToString:@"null"] ? @"" : string;
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if (self.curSysPublic == nil) {
        return;
    }
    
    // 一条消息解析结束
    if ([elementName isEqualToString:@"MESSAGE"]) {
        
        // 消息来源
        NSString *from = @"";
        if (self.curSysPublic.SEND_DEPT_NAME.length > 0) {
            from = [from stringByAppendingString:self.curSysPublic.SEND_DEPT_NAME];
        }
        if (self.curSysPublic.SEND_DUTY_NAME.length > 0) {
            from = [from stringByAppendingString:[NSString stringWithFormat:@"(%@)", self.curSysPublic.SEND_DUTY_NAME]];
        }
        if (self.curSysPublic.SEND_PERSON_NAME.length > 0) {
            if (from.length > 0) {
                from = [from stringByAppendingString:[NSString stringWithFormat:@"-%@", self.curSysPublic.SEND_PERSON_NAME]];
            } else {
                from = [from stringByAppendingString:self.curSysPublic.SEND_PERSON_NAME];
            }
        }
        self.curSysPublic.msgFrom = from;
        
        [self.array addObject:self.curSysPublic];
        return;
    }
    
    if ([elementName isEqualToString:@"MSG_ID"]) {
        self.curSysPublic.msgId = self.tmpStr;
    } else if ([elementName isEqualToString:@"MSG_TITLE"]) {
        self.curSysPublic.msgTitle = self.tmpStr;
    } else if ([elementName isEqualToString:@"MSG_CONTENT"]) {
        self.curSysPublic.msgContent = self.tmpStr;
    } else if ([elementName isEqualToString:@"SEND_DATE"]) {
        self.curSysPublic.SEND_DATE = self.tmpStr;
        if (self.tmpStr.length > 17) {
            self.tmpStr = [self.tmpStr substringToIndex:17];
        }
        self.curSysPublic.msgDate = self.tmpStr;
    } else if ([elementName isEqualToString:@"LAST_DATE"]) {
        self.curSysPublic.LAST_DATE = self.tmpStr;
    } else if ([elementName isEqualToString:@"SYS_CODE"]) {
        self.curSysPublic.SYS_CODE = self.tmpStr;
    } else if ([elementName isEqualToString:@"MSG_TYPE"]) {
        self.curSysPublic.MSG_TYPE = self.tmpStr;
    } else if ([elementName isEqualToString:@"HANDLE_TYPE"]) {
        self.curSysPublic.HANDLE_TYPE = self.tmpStr;
    } else if ([elementName isEqualToString:@"SEND_PERSON"]) {
        self.curSysPublic.SEND_PERSON = self.tmpStr;
    } else if ([elementName isEqualToString:@"SEND_PERSON_NAME"]) {
        self.curSysPublic.SEND_PERSON_NAME = self.tmpStr;
    } else if ([elementName isEqualToString:@"SEND_DEPT"]) {
        self.curSysPublic.SEND_DEPT = self.tmpStr;
    } else if ([elementName isEqualToString:@"SEND_DEPT_NAME"]) {
        self.curSysPublic.SEND_DEPT_NAME = self.tmpStr;
    } else if ([elementName isEqualToString:@"SEND_DUTY"]) {
        self.curSysPublic.SEND_DUTY = self.tmpStr;
    } else if ([elementName isEqualToString:@"SEND_DUTY_NAME"]) {
        self.curSysPublic.SEND_DUTY_NAME = self.tmpStr;
    } else if ([elementName isEqualToString:@"MSG_URL"]) {
        self.curSysPublic.MSG_URL = self.tmpStr;
    } else if ([elementName isEqualToString:@"MSG_URL_PAD"]) {
        self.curSysPublic.MSG_URL_PAD = self.tmpStr;
    } else if ([elementName isEqualToString:@"MSG_URL_PHONE"]) {
        self.curSysPublic.MSG_URL_PHONE = self.tmpStr;
    }
}


- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}

@end
