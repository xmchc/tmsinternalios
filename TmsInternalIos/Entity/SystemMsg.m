//
//  SystemMsg.m
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-28.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "SystemMsg.h"

@interface SystemMsg () <NSXMLParserDelegate>

@property (nonatomic, strong) void(^block)(SystemMsg * msg);
@property (nonatomic, strong) NSMutableString * tmp;
@end

@implementation SystemMsg

- (id)init
{
    if (self = [super init]) {
        self.code = @"-1";
        self.msg = @"";
    }
    return self;
}

- (void)parseData:(NSData *)data complete:(void (^)(SystemMsg *))block
{
    self.block = block;
    self.tmp = [[NSMutableString alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    self.tmp = [[NSMutableString alloc] init];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    [self.tmp appendString:string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"code"]
        || [elementName isEqualToString:@"logonCode"]) {
        self.code = self.tmp;
    } else if ([elementName isEqualToString:@"msg"]
               || [elementName isEqualToString:@"logonMsg"]){
        self.msg = self.tmp;
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self);
}

@end
