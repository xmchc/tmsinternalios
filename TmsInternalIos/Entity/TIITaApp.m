//
//  TIITaApp.m
//  TmsInternalIos
//
//  Created by hsit on 14-8-12.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIITaApp.h"

@interface TIITaApp () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableString * tmp;
@property (nonatomic, strong) NSMutableArray * array;

@end

@implementation TIITaApp

- (void)parseData:(NSData *)data complete:(void (^)(NSArray *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"tabAppModuleType"]) {
        TIITaApp  *item = [[TIITaApp alloc] init];
        [self.array addObject:item];
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    self.tmp = [NSMutableString stringWithString:string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if (self.array.count <= 0) {
        return;
    }
    TIITaApp *item = [self.array objectAtIndex:self.array.count - 1];
    if ([elementName isEqualToString:@"appType"]) {
        item.appType = self.tmp;
    }
}

-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self.array);
}



@end
