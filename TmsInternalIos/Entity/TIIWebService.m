//
//  TIIWebService.m
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-28.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIIWebService.h"

static BiPerson * _PERSON;
static TIIUnreadCount *_UNREADCOUNT;
static NSString *WEBSERVICE_HOST = @"";

@implementation TIIWebService

+ (void)setWebServiceHost:(NSString *)url
{
    WEBSERVICE_HOST = url;
    return;
}

+ (void)setUser:(BiPerson *)person
{
    _PERSON = person;
    NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:[_PERSON toJson] forKey:@"BiPerson"];
    [ud setObject:_PERSON.userName forKey:@"UserName"];
    [ud setObject:_PERSON.password forKey:@"Password"];
    [ud synchronize];
}

+ (BiPerson *)user
{
    if (_PERSON == nil) {
        NSUserDefaults * ud = [NSUserDefaults standardUserDefaults];
        NSString * json = [ud objectForKey:@"BiPerson"];
        NSString * userName = [ud objectForKey:@"UserName"];
        NSString * password = [ud objectForKey:@"Password"];
        if (json == nil) {
            _PERSON = [[BiPerson alloc] init];
        } else {
            _PERSON = [[BiPerson alloc] initWithJson:json];
        }
        userName = userName == nil ? @"" : userName;
        password = password == nil ? @"" : password;
        _PERSON.userName = userName;
        _PERSON.password = password;
    }
    return _PERSON;
}

+ (void)setUnreadCount:(TIIUnreadCount *)unreadCount
{
    _UNREADCOUNT = unreadCount;
    return;
}

+ (TIIUnreadCount *)unreadCount
{
    if (_UNREADCOUNT == nil) {
        _UNREADCOUNT = [[TIIUnreadCount alloc] init];
    }
    return _UNREADCOUNT;
}

+ (NSString *)webServiceHost
{
    return WEBSERVICE_HOST;
}

+ (NSString *)webServiceMainHost
{
    return WEBSERVICE_MAIN_HOST;
}

+ (NSString *)getFactoryiInfoList
{
    return [WEBSERVICE_MAIN_HOST stringByAppendingPathComponent:@"/mobile-web/widePriceCollect/getFactoryInfoList.do"];
}

+ (NSString *)widePriceCollect:(NSString *)userCode password:(NSString *)password
{
    return [WEBSERVICE_MAIN_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/mobile-web/widePriceCollect/loginIn.do?userCode=%@&userPwd=%@",userCode,password]];
}

+ (NSString *)widePriceShow:(NSString *)companyCode
{
    return [WEBSERVICE_MAIN_HOST stringByAppendingPathComponent:[NSString stringWithFormat:@"/mobile-web/pages/widePriceShow.html?companyCode=%@&platform=ios",companyCode]];
}

// soap地址
+ (NSString *)soapUrl
{
    return [[TIIWebService webServiceHost] stringByAppendingPathComponent:@"/MCP_Flex/webServices/IMsgCenterWS"];
}

+ (NSString *)getCustLoginInfo:(NSString *)personMB
{
    return [[TIIWebService webServiceMainHost] stringByAppendingPathComponent:[NSString stringWithFormat:@"/padtmi-lrp/ws/rs/custInfo/getCustLoginInfo/personMB/%@",personMB]];
}

+ (NSString *)loginUrl:(NSString *)userName password:(NSString *)password
{
    return [[TIIWebService webServiceHost] stringByAppendingPathComponent:[NSString stringWithFormat:@"/padtmi/ws/rs/android/queryPassword/userId/%@/password/%@", userName, password]];
}

+ (NSString *)checkUpdateUrl
{
    return [[TIIWebService webServiceHost] stringByAppendingPathComponent:[NSString stringWithFormat:@"/padtmi/ws/rs/moduleUpdate/getModuleUpdate/systemCode/CSAP-REP/moduleCode/%@", APP_TYPE]];
    
}

+ (NSString *)lrpMenuWithTypeUrl:(NSInteger)type
{
    return [[TIIWebService webServiceHost] stringByAppendingPathComponent:[NSString stringWithFormat:@"/padtmi/ws/rs/taAppModule/moduleForType/%d", type]];
}

+ (NSString *)taAppModule
{
    return [[TIIWebService webServiceHost] stringByAppendingPathComponent:@"/padtmi-lrp/ws/rs/taAppModule/moduleTab"];
}

+ (NSString *)getEventUrl
{
    return [[TIIWebService webServiceHost] stringByAppendingPathComponent:@"/mobile-test/pages-lrp/unTreatedWork.html"];
    
}

// 检测待办事物是否处理完的url；
+ (NSString *)getIssueStatusUrl:(NSString *)issueId;
{
    return [[TIIWebService webServiceHost] stringByAppendingPathComponent:[NSString stringWithFormat:@"/padtmi-lrp/ws/rs/affair/getIssueStatus/issueId//%@", issueId]];
}

// getMsgByType的参数，获取消息
+ (NSString *)getMsgByTypeParams:(NSString *)msgType
{
    NSString *params = [NSString stringWithFormat:
                        @"<personCode>%@</personCode>\n"
                        "<personName>%@</personName>\n"
                        "<readed>0</readed>\n"
                        "<msgType>%@</msgType>\n"
                        "<sysCode>107</sysCode>\n"
                        "<beginTime></beginTime>\n"
                        "<endTime></endTime>\n"
                        "<terminalFlag>3</terminalFlag>\n"
                        , [TIIWebService user].personCode, [TIIWebService user].personName, msgType
                        ];
    return params;
}

// checkUnreadCount的参数，获取未读数量
+ (NSString *)checkUnreadCountParams
{
    NSString *params = [NSString stringWithFormat:
                        @"<personCode>%@</personCode>\n"
                        "<personName>%@</personName>\n"
                        "<terminalFlag>3</terminalFlag>\n"
                        , [TIIWebService user].personCode, [TIIWebService user].personName
                        ];
    return params;
}

// updateReadStatus的参数，更新是否已读
+ (NSString *)updateReadStatusParams:(NSString *)msgId
{
    NSString *params = [NSString stringWithFormat:
                        @"<personCode>%@</personCode>\n"
                        "<msgId>%@</msgId>\n"
                        "<flag>1</flag>\n"
                        , [TIIWebService user].personCode, msgId
                        ];
    return params;
}

// 上传deviceToken的参数；
+ (NSString *)upTokenParams:(NSString *)token
{
    NSString *params = [NSString stringWithFormat:
                        @"<personCode>%@</personCode>\n"
                        "<token>%@</token>\n"
                        , [TIIWebService user].personCode, token
                        ];
    return params;
}

@end
