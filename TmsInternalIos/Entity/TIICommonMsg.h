//
//  TIISystemPublic.h
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-21.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TIICommonMsg : NSObject

@property (nonatomic, strong) NSString * msgId;
@property (nonatomic, strong) NSString * msgTitle;
@property (nonatomic, strong) NSString * msgContent;
@property (nonatomic, strong) NSString * msgDate;
@property (nonatomic, strong) NSString * SYS_CODE;
@property (nonatomic, strong) NSString * MSG_TYPE;
@property (nonatomic, strong) NSString * HANDLE_TYPE;
@property (nonatomic, strong) NSString * SEND_DATE;
@property (nonatomic, strong) NSString * LAST_DATE;
@property (nonatomic, strong) NSString * SEND_PERSON;
@property (nonatomic, strong) NSString * SEND_PERSON_NAME;
@property (nonatomic, strong) NSString * SEND_DEPT;
@property (nonatomic, strong) NSString * SEND_DEPT_NAME;
@property (nonatomic, strong) NSString * SEND_DUTY;
@property (nonatomic, strong) NSString * SEND_DUTY_NAME;
@property (nonatomic, strong) NSString * MSG_URL;
@property (nonatomic, strong) NSString * MSG_URL_PAD;
@property (nonatomic, strong) NSString * MSG_URL_PHONE;

@property (nonatomic) BOOL expand;
@property (nonatomic, strong) NSString *msgFrom;

- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;

@end
