//
//  RYKDeviceToken.m
//  RYKForCustIos
//
//  Created by 张斌 on 14-8-1.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIIDeviceToken.h"

static TIIDeviceToken * DEVICETOKEN; // 当前用户

@implementation TIIDeviceToken

+ (TIIDeviceToken *)sharedManager
{
    if (DEVICETOKEN == nil) {
        DEVICETOKEN = [[TIIDeviceToken alloc] init];
        DEVICETOKEN.deviceToken = @"";
    }
    return DEVICETOKEN;
}


@end
