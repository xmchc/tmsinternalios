//
//  TIILoginInfo.m
//  TmsInternalIos
//
//  Created by hsit on 14-8-11.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIILoginInfo.h"

@interface TIILoginInfo () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableString * tmp;

@end


@implementation TIILoginInfo


- (void)parseData:(NSData *)data
{
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"CustLoginInfo"]) {
        self.tmp = [[NSMutableString alloc] init];
    }

}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    self.tmp = [NSMutableString stringWithString:string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"orgUrl"]) {
        self.orgUrl = self.tmp;
    } else if ([elementName isEqualToString:@"r1Person"]){
        self.r1Person = self.tmp;
    } else if ([elementName isEqualToString:@"orgCode"]){
        self.orgCode = self.tmp;
    }
}


@end
