//
//  TIILoginInfo.h
//  TmsInternalIos
//
//  Created by hsit on 14-8-11.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TIILoginInfo : NSObject

@property (nonatomic, strong) NSString * orgUrl;
@property (nonatomic, strong) NSString * r1Person;
@property (nonatomic, strong) NSString * orgCode;

- (void)parseData:(NSData *)data;


@end
