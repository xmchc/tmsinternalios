//
//  BiPerson.h
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-28.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BiPerson : NSObject


@property (nonatomic, strong) NSString * userName;     // 用户名
@property (nonatomic, strong) NSString * password;     // 密码
@property (nonatomic, strong) NSString * personCode;   // 人员代码
@property (nonatomic, strong) NSString * personName;   // 人员名称
@property (nonatomic, strong) NSString * sessionCode;  // 会话id
@property (nonatomic, strong) NSString * deptCode;     // 部门代码
@property (nonatomic, strong) NSString * deptName;     // 部门名称
@property (nonatomic, strong) NSString * duty;         // 岗位代码
@property (nonatomic, strong) NSString * dutyName;     // 岗位名称
@property (nonatomic, strong) NSString * orgCode;      // 公司代码
@property (nonatomic, strong) NSString * orgName;      // 公司名称
@property (nonatomic, strong) NSString * personMb;     // 移动电话
@property (nonatomic, strong) NSString * personPhoneNumber;  // 联系电话
@property (nonatomic, strong) NSString * sex;          // 性别
@property (nonatomic) BOOL sessionLogin;

- (id)initWithJson:(NSString *)json;
- (void)parseData:(NSData *)data complete:(void(^)(NSArray *array))block;
- (BOOL)isLogin;
- (NSString *)toJson;

@end
