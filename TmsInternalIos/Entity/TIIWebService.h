//
//  TIIWebService.h
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-28.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BiPerson.h"
#import "TIIUnreadCount.h"

#define APP_TYPE @"200"   // 200 正版  300 越狱

#define WEBSERVICE_MAIN_HOST @"http://183.250.160.9"

#define SOAP_NAMESPACE @"http://webService.icsshs.com/"
// 调试模式，发布一定要关闭
#define IS_DEBUG_MODE @"0"
// 是否开启工业用户
#define IS_OPEN_GY @"0"

@interface TIIWebService : NSObject

+ (void)setWebServiceHost:(NSString *)url;
+ (void)setUser:(BiPerson *)person;
+ (BiPerson *)user;
+ (void)setUnreadCount:(TIIUnreadCount *)unreadCount;
+ (TIIUnreadCount *)unreadCount;
// 主机地址
+ (NSString *)webServiceHost;
// 主机登陆验证地址
+ (NSString *)webServiceMainHost;
// 工业客户列表
+ (NSString *)getFactoryiInfoList;
// 工业客户登录验证
+ (NSString *)widePriceCollect:(NSString *)userCode password:(NSString *)password;
// 工业客户主页url
+ (NSString *)widePriceShow:(NSString *)companyCode;
// soap地址
+ (NSString *)soapUrl;
// 验证登陆接口
+ (NSString *)getCustLoginInfo:(NSString *)personMB;
// 登录地址
+ (NSString *)loginUrl:(NSString *)userName password:(NSString *)password;
// 检查版本更新
+ (NSString *)checkUpdateUrl;
// 报表列表
+ (NSString *)lrpMenuWithTypeUrl:(NSInteger)type;
// 获取Tab页
+ (NSString *)taAppModule;
// 未处理事务的url；
+ (NSString *)getEventUrl;
// 检测待办事物是否处理完的url；
+ (NSString *)getIssueStatusUrl:(NSString *)issueId;


// getMsgByType的参数，获取消息
+ (NSString *)getMsgByTypeParams:(NSString *)msgType;
// checkUnreadCount的参数，获取未读数量
+ (NSString *)checkUnreadCountParams;
// updateReadStatus的参数，更新是否已读
+ (NSString *)updateReadStatusParams:(NSString *)msgId;
// 上传deviceToken的参数；
+ (NSString *)upTokenParams:(NSString *)token;

@end
