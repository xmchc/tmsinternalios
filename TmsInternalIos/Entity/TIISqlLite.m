//
//  TIISqlLite.m
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-24.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIISqlLite.h"
#import "FMDB.h"
#import "TIICommonMsg.h"
#import "TIIWebService.h"

#define MSG_TABLE_NAME @"MsgData"

#define FMDBQuickCheck(SomeBool) { if (!(SomeBool)) { NSLog(@"Failure on line %d", __LINE__); } }

static TIISqlLite * _SQLLITEMANAGER;

@interface SearchCondition ()

@end

@implementation SearchCondition

- (id)init
{
    if (self = [super init]) {
        self.beginDate = @"";
        self.endDate = @"";
        self.msgType = @"1";
        self.keyWords = @"";
    }
    return self;
}

@end

@interface TIISqlLite ()

- (NSString *)dbPath;

@end

@implementation TIISqlLite

+ (TIISqlLite *)shareManager
{
    if (_SQLLITEMANAGER == nil) {
        _SQLLITEMANAGER = [[TIISqlLite alloc] init];
    }
    return  _SQLLITEMANAGER;
}

- (NSString *)dbPath
{
    NSString* docsdir = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *dbFileName = [NSString stringWithFormat:@"%@.sqlite", [TIIWebService user].userName];
    NSString* dbpath = [docsdir stringByAppendingPathComponent:dbFileName];
    return dbpath;
}

- (FMDatabase *)openDB
{
    NSString *dbPath = [self dbPath];
    FMDatabase *db= [FMDatabase databaseWithPath:dbPath];
    if (![db open]) {
        NSLog(@"Could not open db.");
        return nil;
    }
    return db;
}

- (void)createMsgTable
{
    FMDatabase *db = [self openDB];
    if (db == nil) {
        return;
    }
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS MsgData (msgId text PRIMARY KEY,msgTitle text,msgContent text,msgDate text,SYS_CODE text,MSG_TYPE text,HANDLE_TYPE text,SEND_DATE text,LAST_DATE text,SEND_PERSON text,SEND_PERSON_NAME text,SEND_DEPT text,SEND_DEPT_NAME text,SEND_DUTY text,SEND_DUTY_NAME text,MSG_URL text,MSG_URL_PAD text,MSG_URL_PHONE text)"];
    [db close];
    return;
}

- (void)addNewMsg:(NSArray *)msgArray
{
    FMDatabase *db = [self openDB];
    if (db == nil) {
        return;
    }
    for (int i = 0; i < msgArray.count; i++) {
        TIICommonMsg *curMsg = [msgArray objectAtIndex:i];
        NSMutableDictionary *dictionaryArgs = [NSMutableDictionary dictionary];
        [dictionaryArgs setObject:curMsg.msgId forKey:@"msgId"];
        [dictionaryArgs setObject:curMsg.msgTitle forKey:@"msgTitle"];
        [dictionaryArgs setObject:curMsg.msgContent forKey:@"msgContent"];
        [dictionaryArgs setObject:curMsg.msgDate forKey:@"msgDate"];
        [dictionaryArgs setObject:curMsg.SYS_CODE forKey:@"SYS_CODE"];
        [dictionaryArgs setObject:curMsg.MSG_TYPE forKey:@"MSG_TYPE"];
        [dictionaryArgs setObject:curMsg.HANDLE_TYPE forKey:@"HANDLE_TYPE"];
        [dictionaryArgs setObject:curMsg.SEND_DATE forKey:@"SEND_DATE"];
        [dictionaryArgs setObject:curMsg.LAST_DATE forKey:@"LAST_DATE"];
        [dictionaryArgs setObject:curMsg.SEND_PERSON forKey:@"SEND_PERSON"];
        [dictionaryArgs setObject:curMsg.SEND_PERSON_NAME forKey:@"SEND_PERSON_NAME"];
        [dictionaryArgs setObject:curMsg.SEND_DEPT forKey:@"SEND_DEPT"];
        [dictionaryArgs setObject:curMsg.SEND_DEPT_NAME forKey:@"SEND_DEPT_NAME"];
        [dictionaryArgs setObject:curMsg.SEND_DUTY forKey:@"SEND_DUTY"];
        [dictionaryArgs setObject:curMsg.SEND_DUTY_NAME forKey:@"SEND_DUTY_NAME"];
        [dictionaryArgs setObject:curMsg.MSG_URL forKey:@"MSG_URL"];
        [dictionaryArgs setObject:curMsg.MSG_URL_PAD forKey:@"MSG_URL_PAD"];
        [dictionaryArgs setObject:curMsg.MSG_URL_PHONE forKey:@"MSG_URL_PHONE"];
        FMDBQuickCheck([db executeUpdate:@"insert into MsgData values (:msgId, :msgTitle, :msgContent, :msgDate, :SYS_CODE, :MSG_TYPE, :HANDLE_TYPE, :SEND_DATE, :LAST_DATE, :SEND_PERSON, :SEND_PERSON_NAME, :SEND_DEPT, :SEND_DEPT_NAME, :SEND_DUTY, :SEND_DUTY_NAME, :MSG_URL, :MSG_URL_PAD, :MSG_URL_PHONE)" withParameterDictionary:dictionaryArgs]);
    }
    [db close];
    return;
}

- (void)deleteMsgByMsgId:(NSString *)msgId
{
    FMDatabase *db = [self openDB];
    if (db == nil) {
        return;
    }
    
    [db executeUpdate:@"delete from MsgData WHERE msgId=?", msgId];
    
    [db close];
    return;
}

- (void)deleteMsgByIssueId:(NSString *)issueId
{
    FMDatabase *db = [self openDB];
    if (db == nil) {
        return;
    }
    
    NSString *issue = [NSString stringWithFormat:@"SWDB+%@", issueId];
    [db executeUpdate:@"delete from MsgData WHERE MSG_URL_PAD=?", issue];
    [db close];
    return;
}

- (NSString *)getLastMonthDateStr
{
    NSDate *nowDate = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps;
    comps = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:[[NSDate alloc] init]];
    [comps setHour:0];
    [comps setMinute:0];
    [comps setSecond:0];
    [comps setDay:-30];
    NSDate *tagDate = [calendar dateByAddingComponents:comps toDate:nowDate options:0];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd 00:00:00"];
    NSString *destDateString = [dateFormatter stringFromDate:tagDate];
    return destDateString;
}

- (NSArray *)getMsgByType:(NSString *)msgType
{
    NSMutableArray *msgArray = [[NSMutableArray alloc] init];
    FMDatabase *db = [self openDB];
    if (db == nil) {
        return msgArray;
    }
    
    NSString *lastMonth = [self getLastMonthDateStr];
    FMResultSet *rs = [db executeQueryWithFormat:@"SELECT * FROM MsgData WHERE SEND_DATE > %@ AND MSG_TYPE = %@ ORDER BY SEND_DATE desc", lastMonth, msgType];
    while ([rs next]) {
        TIICommonMsg *curMsg = [[TIICommonMsg alloc] init];
        curMsg.msgId = [rs stringForColumn:@"msgId"];
        curMsg.msgTitle = [rs stringForColumn:@"msgTitle"];
        curMsg.msgContent = [rs stringForColumn:@"msgContent"];
        curMsg.msgDate = [rs stringForColumn:@"msgDate"];
        curMsg.SYS_CODE = [rs stringForColumn:@"SYS_CODE"];
        curMsg.MSG_TYPE = [rs stringForColumn:@"MSG_TYPE"];
        curMsg.HANDLE_TYPE = [rs stringForColumn:@"HANDLE_TYPE"];
        curMsg.SEND_DATE = [rs stringForColumn:@"SEND_DATE"];
        curMsg.LAST_DATE = [rs stringForColumn:@"LAST_DATE"];
        curMsg.SEND_PERSON = [rs stringForColumn:@"SEND_PERSON"];
        curMsg.SEND_PERSON_NAME = [rs stringForColumn:@"SEND_PERSON_NAME"];
        curMsg.SEND_DEPT = [rs stringForColumn:@"SEND_DEPT"];
        curMsg.SEND_DEPT_NAME = [rs stringForColumn:@"SEND_DEPT_NAME"];
        curMsg.SEND_DUTY = [rs stringForColumn:@"SEND_DUTY"];
        curMsg.SEND_DUTY_NAME = [rs stringForColumn:@"SEND_DUTY_NAME"];
        curMsg.MSG_URL = [rs stringForColumn:@"MSG_URL"];
        curMsg.MSG_URL_PAD = [rs stringForColumn:@"MSG_URL_PAD"];
        curMsg.MSG_URL_PHONE = [rs stringForColumn:@"MSG_URL_PHONE"];
        [msgArray addObject:curMsg];
    }
    [rs close];
    [db close];
    return msgArray;
}

- (NSArray *)getMsgByCondition:(SearchCondition *)condition
{
    NSMutableArray *msgArray = [[NSMutableArray alloc] init];
    FMDatabase *db = [self openDB];
    if (db == nil) {
        return msgArray;
    }
    
    FMResultSet *rs;
    if (condition.keyWords.length > 0) {
        // 有关键字查询
        NSString *keyFormat = @"%";
        keyFormat = [keyFormat stringByAppendingString:condition.keyWords];
        keyFormat = [keyFormat stringByAppendingString:@"%"];
        rs = [db executeQuery:@"SELECT * FROM MsgData WHERE SEND_DATE > ? AND SEND_DATE < ? AND MSG_TYPE = ? AND (msgTitle LIKE ? OR msgContent LIKE ?) ORDER BY SEND_DATE desc", condition.beginDate, condition.endDate, condition.msgType, keyFormat, keyFormat];
    } else {
        // 无关键字查询
        rs = [db executeQueryWithFormat:@"SELECT * FROM MsgData WHERE SEND_DATE > %@ AND SEND_DATE < %@ AND MSG_TYPE = %@ ORDER BY SEND_DATE desc", condition.beginDate, condition.endDate, condition.msgType];
    }
    while ([rs next]) {
        TIICommonMsg *curMsg = [[TIICommonMsg alloc] init];
        curMsg.msgId = [rs stringForColumn:@"msgId"];
        curMsg.msgTitle = [rs stringForColumn:@"msgTitle"];
        curMsg.msgContent = [rs stringForColumn:@"msgContent"];
        curMsg.msgDate = [rs stringForColumn:@"msgDate"];
        curMsg.SYS_CODE = [rs stringForColumn:@"SYS_CODE"];
        curMsg.MSG_TYPE = [rs stringForColumn:@"MSG_TYPE"];
        curMsg.HANDLE_TYPE = [rs stringForColumn:@"HANDLE_TYPE"];
        curMsg.SEND_DATE = [rs stringForColumn:@"SEND_DATE"];
        curMsg.LAST_DATE = [rs stringForColumn:@"LAST_DATE"];
        curMsg.SEND_PERSON = [rs stringForColumn:@"SEND_PERSON"];
        curMsg.SEND_PERSON_NAME = [rs stringForColumn:@"SEND_PERSON_NAME"];
        curMsg.SEND_DEPT = [rs stringForColumn:@"SEND_DEPT"];
        curMsg.SEND_DEPT_NAME = [rs stringForColumn:@"SEND_DEPT_NAME"];
        curMsg.SEND_DUTY = [rs stringForColumn:@"SEND_DUTY"];
        curMsg.SEND_DUTY_NAME = [rs stringForColumn:@"SEND_DUTY_NAME"];
        curMsg.MSG_URL = [rs stringForColumn:@"MSG_URL"];
        curMsg.MSG_URL_PAD = [rs stringForColumn:@"MSG_URL_PAD"];
        curMsg.MSG_URL_PHONE = [rs stringForColumn:@"MSG_URL_PHONE"];
        [msgArray addObject:curMsg];
    }
    [rs close];
    [db close];
    return msgArray;
}

- (SearchCondition *)getInfoByCondition:(NSString *)year withWeek:(NSString *)week
{
    NSString *dbpath = [[NSBundle mainBundle] pathForResource:@"hsit_base" ofType:@"db"];
    FMDatabase *db = [FMDatabase databaseWithPath:dbpath];
    SearchCondition *curDate = [[SearchCondition alloc] init];
    if ([db open]) {
        FMResultSet * rs;
        rs = [db executeQuery:@"SELECT * FROM bi_year_week WHERE year = ? AND week = ?", year, week];
        while ([rs next]) {
            curDate.beginDate = [rs stringForColumn:@"start_date"];
            curDate.endDate = [rs stringForColumn:@"end_date"];
        }
        [rs close];
        [db close];
    }
    return curDate;
}

@end
