//
//  TIIUnreadCount.m
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-23.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIIUnreadCount.h"

@interface TIIUnreadCount () <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) void(^block)(TIIUnreadCount *unreadCount);
@property (nonatomic, strong) NSString *tmpStr;

@end

@implementation TIIUnreadCount

- (id)init
{
    if (self = [super init]) {
        self.unreadSysPublicCnt = 0;
        self.unreadChatCnt = 0;
        self.unreadToDoCnt = 0;
        self.unreadWarningCnt = 0;
        self.unreadRemindCnt = 0;
    }
    return self;
}

- (void)parseData:(NSData *)data complete:(void (^)(TIIUnreadCount *))block
{
    self.block = block;
    self.array = [[NSMutableArray alloc] init];
    
    NSXMLParser * parser = [[NSXMLParser alloc] initWithData:data];
    parser.delegate = self;
    [parser parse];
}

#pragma mark -
#pragma mark NSXMLParser Delegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"MESSAGE"]) {
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    self.tmpStr = string;
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"ZM_DB"]) {
        self.unreadToDoCnt += [self.tmpStr intValue];
    } else if ([elementName isEqualToString:@"WL_DB"]) {
        self.unreadToDoCnt += [self.tmpStr intValue];
    } else if ([elementName isEqualToString:@"YX_DB"]) {
        self.unreadToDoCnt += [self.tmpStr intValue];
    } else if ([elementName isEqualToString:@"ZM_YJ"]) {
        self.unreadWarningCnt += [self.tmpStr intValue];
    } else if ([elementName isEqualToString:@"WL_YJ"]) {
        self.unreadWarningCnt += [self.tmpStr intValue];
    } else if ([elementName isEqualToString:@"YX_YJ"]) {
        self.unreadWarningCnt += [self.tmpStr intValue];
    } else if ([elementName isEqualToString:@"ZM_TX"]) {
        self.unreadRemindCnt += [self.tmpStr intValue];
    } else if ([elementName isEqualToString:@"WL_TX"]) {
        self.unreadRemindCnt += [self.tmpStr intValue];
    } else if ([elementName isEqualToString:@"YX_TX"]) {
        self.unreadRemindCnt += [self.tmpStr intValue];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    self.block(self);
}

@end
