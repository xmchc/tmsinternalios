//
//  TIIAppDelegate.h
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-28.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TIIViewController.h"

@interface TIIAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) TIIViewController * controller;

@end
