//
//  TIIRemindViewController.h
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-21.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTViewController.h"

@class TIIRemindViewController;

@protocol TIIRemindViewControllerDelegate <NSObject>

@optional
- (void)tiiRemindViewControllerDelegate:(TIIRemindViewController *)controller pushController:(UIViewController *)tagController;

@end

@interface TIIRemindViewController : CTViewController

@property (nonatomic, weak) id<TIIRemindViewControllerDelegate> delegate;

@end
