//
//  TIIMsgController.m
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-28.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIIMsgController.h"
#import "TIIMyCareViewController.h"
#import "TIIToDoViewController.h"
#import "TIIWarningViewController.h"
#import "TIIRemindViewController.h"
#import "TIIChatViewController.h"
#import "TIIDebugSendMsgViewController.h"
#import "TIIWebService.h"
#import "TIISearchViewController.h"

@interface TIIMsgController () <TIIMyCareViewControllerDelegate, TIIRemindViewControllerDelegate, TIIWarningViewControllerDelegate, TIIToDoViewControllerDelegate>

@property (nonatomic, strong) IBOutlet UIView *topView;
@property (nonatomic, strong) IBOutlet UIView *topViewSelectLine;
@property (nonatomic, strong) TIIMyCareViewController *myCareViewController;    // 我的关注
@property (nonatomic, strong) TIIChatViewController *chatViewController;      // 即时消息
@property (nonatomic, strong) TIIToDoViewController *todoViewController;      // 待办业务
@property (nonatomic, strong) TIIWarningViewController *warningViewController;   // 预警
@property (nonatomic, strong) TIIRemindViewController *remindViewController;    // 提醒
@property (nonatomic, strong) IBOutlet UIImageView *imgMyCareNew;
@property (nonatomic, strong) IBOutlet UIImageView *imgToDoNew;
@property (nonatomic, strong) IBOutlet UIImageView *imgChatNew;
@property (nonatomic, strong) IBOutlet UIImageView *imgWarningNew;
@property (nonatomic, strong) IBOutlet UIImageView *imgRemindNew;
@property (nonatomic) NSInteger curSelectIndex;

- (IBAction)topControlClicked:(UIControl *)sender;
- (void)onMsgCountNof;

@end

@implementation TIIMsgController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setTabBarItemWithTitle:@"消息" titleColor:[UIColor colorWithRed:153.0/255.0 green:88.0/255.0 blue:163.0/255.0 alpha:1] titleSelectedColor:[UIColor whiteColor] image:[UIImage imageNamed:@"TIITabMsg"] selectedImage:[UIImage imageNamed:@"TIITabMsgHighlighted"]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 消息数量通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMsgCountNof) name:NOTIFCATION_MESSAGE_COUNT object:nil];
    
    self.curSelectIndex = -1;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.title = self.title;
    
    UIButton *btnhelp = [UIButton buttonWithType:UIButtonTypeCustom];
    btnhelp.frame = CGRectMake(0, 0, 22, 22);
    [btnhelp setImage:[UIImage imageNamed:@"h_help"] forState:UIControlStateNormal];
    [btnhelp addTarget:self action:@selector(helpclick:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *btnsearch = [UIButton buttonWithType:UIButtonTypeCustom];
    btnsearch.frame = CGRectMake(35, 0, 22, 22);
    if ([IS_DEBUG_MODE isEqualToString:@"1"]) {
        btnhelp.hidden = NO;
    } else {
        btnhelp.hidden = YES;
    }

    btnsearch.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"h_search"]];
    [btnsearch addTarget:self action:@selector(searchclick:) forControlEvents:UIControlEventTouchUpInside];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 62, 22)];
    view.backgroundColor = [UIColor clearColor];
    [view addSubview:btnhelp];
    [view addSubview:btnsearch];
    UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.tabBarController.navigationItem.rightBarButtonItem = bar;
    
    
    if (self.curSelectIndex == -1) {
        NSInteger subViewY = CGRectGetMaxY(self.topView.frame);
        CGRect subViewFrame = CGRectMake(0, subViewY, self.view.bounds.size.width, self.view.bounds.size.height - subViewY);
        
        self.myCareViewController = [[TIIMyCareViewController alloc] initWithNibName:@"TIIMyCareViewController" bundle:nil];
        self.myCareViewController.view.frame = subViewFrame;
        self.myCareViewController.delegate = self;
        
        self.chatViewController = [[TIIChatViewController alloc] initWithNibName:@"TIIChatViewController" bundle:nil];
        self.chatViewController.view.frame = subViewFrame;
        
        self.todoViewController = [[TIIToDoViewController alloc] initWithNibName:@"TIIToDoViewController" bundle:nil];
        self.todoViewController.view.frame = subViewFrame;
        self.todoViewController.delegate = self;
        
        self.warningViewController = [[TIIWarningViewController alloc] initWithNibName:@"TIIWarningViewController" bundle:nil];
        self.warningViewController.view.frame = subViewFrame;
        self.warningViewController.delegate = self;
        
        self.remindViewController = [[TIIRemindViewController alloc] initWithNibName:@"TIIRemindViewController" bundle:nil];
        self.remindViewController.view.frame = subViewFrame;
        self.remindViewController.delegate = self;
        
        [self.view addSubview:self.myCareViewController.view];
        [self.view addSubview:self.chatViewController.view];
        [self.view addSubview:self.todoViewController.view];
        [self.view addSubview:self.warningViewController.view];
        [self.view addSubview:self.remindViewController.view];
        
        [self topControlClicked:nil];
        [self onMsgCountNof];
    } else {
        if (self.curSelectIndex == 1) {
            //  待办业务
            self.todoViewController.view.hidden = NO;
            [self.todoViewController viewWillAppear:NO];
        } else if (self.curSelectIndex == 2) {
            // 预警
            self.warningViewController.view.hidden = NO;
            [self.warningViewController viewWillAppear:NO];
        } else if (self.curSelectIndex == 3) {
            // 提醒
            self.remindViewController.view.hidden = NO;
            [self.remindViewController viewWillAppear:NO];
        }
    }
}

- (void)onMsgCountNof
{
    TIIUnreadCount *unreadCount = [TIIWebService unreadCount];
    self.imgMyCareNew.hidden = unreadCount.unreadSysPublicCnt <= 0;
    self.imgChatNew.hidden = unreadCount.unreadChatCnt <= 0;
    self.imgToDoNew.hidden = unreadCount.unreadToDoCnt <= 0;
    self.imgWarningNew.hidden = unreadCount.unreadWarningCnt <= 0;
    self.imgRemindNew.hidden = unreadCount.unreadRemindCnt <= 0;
    return;
}

#pragma mark -
#pragma mark click event
- (IBAction)topControlClicked:(UIControl *)sender
{
    NSInteger selectIndex = 1;
    if (sender != nil) {
        selectIndex = sender.tag;
    }
    
    // 修改选中线条位置
    CGRect topLineFrame = self.topViewSelectLine.frame;
    self.curSelectIndex = selectIndex;
    if (sender == nil) {
        topLineFrame.origin.x = 0;
    } else {
        topLineFrame.origin.x = (self.curSelectIndex - 1) * sender.frame.size.width;
    }
    self.topViewSelectLine.frame = topLineFrame;
    
    self.myCareViewController.view.hidden = YES;
    self.chatViewController.view.hidden = YES;
    self.todoViewController.view.hidden = YES;
    self.warningViewController.view.hidden = YES;
    self.remindViewController.view.hidden = YES;
//    if (self.curSelectIndex == 1) {
//        // 我的关注
//        self.myCareViewController.view.hidden = NO;
//        [self.myCareViewController viewWillAppear:NO];
//    } else if (self.curSelectIndex == 2) {
//        // 即时消息
//        self.chatViewController.view.hidden = NO;
//        [self.chatViewController viewWillAppear:NO];
//    } else if (self.curSelectIndex == 3) {
//        //  待办业务
//        self.todoViewController.view.hidden = NO;
//        [self.todoViewController viewWillAppear:NO];
//    } else if (self.curSelectIndex == 4) {
//        // 预警
//        self.warningViewController.view.hidden = NO;
//        [self.warningViewController viewWillAppear:NO];
//    } else if (self.curSelectIndex == 5) {
//        // 提醒
//        self.remindViewController.view.hidden = NO;
//        [self.remindViewController viewWillAppear:NO];
//    }
    if (self.curSelectIndex == 1) {
        //  待办业务
        self.todoViewController.view.hidden = NO;
        [self.todoViewController viewWillAppear:NO];
    } else if (self.curSelectIndex == 2) {
        // 预警
        self.warningViewController.view.hidden = NO;
        [self.warningViewController viewWillAppear:NO];
    } else if (self.curSelectIndex == 3) {
        // 提醒
        self.remindViewController.view.hidden = NO;
        [self.remindViewController viewWillAppear:NO];
    }

    return;
}

- (void)helpclick:(UIButton *)btn
{
    if ([IS_DEBUG_MODE isEqualToString:@"1"]) {
        TIIDebugSendMsgViewController *controller = [[TIIDebugSendMsgViewController alloc] initWithNibName:@"TIIDebugSendMsgViewController" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
        return;
    }
}

- (void)searchclick:(UIButton *)btn
{
    TIISearchViewController *controller = [[TIISearchViewController alloc] initWithNibName:@"TIISearchViewController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -
#pragma mark TIIMyCareViewController delegate
- (void)tiiMyCareViewControllerDelegate:(TIIMyCareViewController *)controller pushController:(UIViewController *)tagController
{
    [self.navigationController pushViewController:tagController animated:YES];
    return;
}

#pragma mark -
#pragma mark TIIRemindViewController delegate
- (void)tiiRemindViewControllerDelegate:(TIIRemindViewController *)controller pushController:(UIViewController *)tagController
{
    [self.navigationController pushViewController:tagController animated:YES];
    return;
}

#pragma mark -
#pragma mark TIIWarningViewController delegate
- (void)tiiWarningViewControllerDelegate:(TIIWarningViewController *)controller pushController:(UIViewController *)tagController
{
    [self.navigationController pushViewController:tagController animated:YES];
    return;
}

#pragma mark -
#pragma mark TIIToDoViewController delegate
- (void)tiiToDoViewControllerDelegate:(TIIToDoViewController *)controller pushController:(UIViewController *)tagController
{
    [self.navigationController pushViewController:tagController animated:YES];
    return;
}

- (void)tiiToDoViewControllerDelegate:(TIIToDoViewController *)controller presentController:(UIViewController *)tagController
{
    [self.navigationController presentViewController:tagController animated:YES completion:nil];
    return;
}


@end
