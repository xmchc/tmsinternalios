//
//  TIIWarningViewController.h
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-21.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTViewController.h"

@class TIIWarningViewController;

@protocol TIIWarningViewControllerDelegate <NSObject>

@optional
- (void)tiiWarningViewControllerDelegate:(TIIWarningViewController *)controller pushController:(UIViewController *)tagController;

@end

@interface TIIWarningViewController : CTViewController

@property (nonatomic, weak) id<TIIWarningViewControllerDelegate> delegate;

@end
