//
//  TIIMyCareViewController.h
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-21.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTViewController.h"

@class TIIMyCareViewController;

@protocol TIIMyCareViewControllerDelegate <NSObject>

@optional
- (void)tiiMyCareViewControllerDelegate:(TIIMyCareViewController *)controller pushController:(UIViewController *)tagController;

@end

@interface TIIMyCareViewController : CTViewController

@property (nonatomic, weak) id<TIIMyCareViewControllerDelegate> delegate;

@end
