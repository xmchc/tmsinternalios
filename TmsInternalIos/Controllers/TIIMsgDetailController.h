//
//  ConsMsgDetailController.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-3.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import "CTViewController.h"
#import "TIICommonMsg.h"

@interface TIIMsgDetailController : CTViewController

@property (nonatomic, weak) TIICommonMsg *curSysPublic;

@end
