//
//  TIISearchViewController.m
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-30.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIISearchViewController.h"
#import "TIICommonMsg.h"
#import "TIISqlLite.h"
#import "TIIDatePicker.h"
#import "CTPikcerView.h"
#import "CTTableView.h"
#import "TIIMsgDetailController.h"

@interface TIISearchViewController () <CTTableViewDelegate, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, TIIDatePickerDelegate, CTPikcerViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UIView *tvHeader;
@property (nonatomic, strong) IBOutlet UIView *viewHeader;
@property (nonatomic, strong) IBOutlet UILabel *labResultCnt;
@property (nonatomic, strong) IBOutlet UILabel *labBeginDate;
@property (nonatomic, strong) IBOutlet UILabel *labEndDate;
@property (nonatomic, strong) IBOutlet UILabel *labMsgType;
@property (nonatomic, strong) IBOutlet UITextField *tfKeyWords;

@property (nonatomic, strong) SearchCondition *condition;
@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSMutableArray *array;

- (IBAction)chooseBeginDate:(id)sender;
- (IBAction)chooseEndDate:(id)sender;
- (IBAction)chooseMsgType:(id)sender;
- (void)helpClick;
- (void)searchClick;

@end

@implementation TIISearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"消息搜索";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton *btnhelp = [UIButton buttonWithType:UIButtonTypeCustom];
    btnhelp.frame = CGRectMake(0, 0, 22, 22);
    [btnhelp setImage:[UIImage imageNamed:@"h_help"] forState:UIControlStateNormal];
    [btnhelp addTarget:self action:@selector(helpClick) forControlEvents:UIControlEventTouchUpInside];
    UIButton *btnsearch = [UIButton buttonWithType:UIButtonTypeCustom];
    btnsearch.frame = CGRectMake(35, 0, 22, 22);
    btnhelp.hidden = YES;
    
    btnsearch.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"h_search"]];
    [btnsearch addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 62, 22)];
    view.backgroundColor = [UIColor clearColor];
    [view addSubview:btnhelp];
    [view addSubview:btnsearch];
    UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithCustomView:view];
    self.navigationItem.rightBarButtonItem = bar;
    
    UIImageView *ivLine1 = [[UIImageView alloc] init];
    ivLine1.image = [[UIImage imageNamed:@"Xuxian"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    ivLine1.frame = CGRectMake(0, 46, ScreenWidth, 1);
    UIImageView *ivLine2 = [[UIImageView alloc] init];
    ivLine2.image = [[UIImage imageNamed:@"Xuxian"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    ivLine2.frame = CGRectMake(0, 86, ScreenWidth, 1);
    UIImageView *ivLine3 = [[UIImageView alloc] init];
    ivLine3.image = [[UIImage imageNamed:@"Xuxian"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    ivLine3.frame = CGRectMake(0, 126, ScreenWidth, 1);
    [self.viewHeader addSubview:ivLine1];
    [self.viewHeader addSubview:ivLine2];
    [self.viewHeader addSubview:ivLine3];
    self.tfKeyWords.delegate = self;
    
    self.array = [[NSMutableArray alloc] init];
    self.condition = [[SearchCondition alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.tv == nil) {
        CGRect tvFrame = CGRectMake(0, self.viewHeader.frame.size.height, ScreenWidth, self.view.bounds.size.height - self.viewHeader.frame.size.height);
        self.tv = [[CTTableView alloc] initWithFrame:tvFrame];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.pullUpViewEnabled = NO;
        self.tv.pullDownViewEnabled = NO;
        self.tv.ctTableViewDelegate = self;
        self.tv.dataSource = self;
        self.tv.delegate = self;
        [self.view addSubview:self.tv];
        
        UIImageView *ivLine = [[UIImageView alloc] init];
        ivLine.image = [[UIImage imageNamed:@"Xuxian"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        ivLine.frame = CGRectMake(15, 45, ScreenWidth - 30, 1);
        [self.tvHeader addSubview:ivLine];
        [self.tv setTableHeaderView:self.tvHeader];
    }
}

#pragma mark click event
- (void)helpClick
{
    return;
}

- (void)searchClick
{
    [self.tfKeyWords resignFirstResponder];
    if ([self.indicator showing]) {
        return;
    }
    
    if (self.condition.beginDate.length <= 0) {
        self.indicator.text = @"请选择开始时间";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    if (self.condition.endDate.length <= 0) {
        self.indicator.text = @"请选择结束时间";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
        return;
    }
    
    self.indicator.text = @"正在搜索...";
    [self.indicator show];
    
    self.condition.keyWords = [self.tfKeyWords.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSArray *allMsgArray = [[TIISqlLite shareManager] getMsgByCondition:self.condition];
    [self.array removeAllObjects];
    self.array = [NSMutableArray arrayWithArray:allMsgArray];
    self.labResultCnt.text = [NSString stringWithFormat:@"搜索结果［共%d条］", self.array.count];
    [self.tv reloadData];
    self.indicator.text = [NSString stringWithFormat:@"搜索完成，搜索结果%d条", self.array.count];
    [self.indicator autoHide:CTIndicateStateDone afterDelay:1];
    return;
}

- (IBAction)chooseBeginDate:(id)sender
{
    [self.tfKeyWords resignFirstResponder];
    TIIDatePicker *picker = [[TIIDatePicker alloc] initWithDateFormatter:@"YYYY-MM-dd"];
    picker.delegate = self;
    picker.tag = 1;
    [picker show];
    return;
}

- (IBAction)chooseEndDate:(id)sender
{
    [self.tfKeyWords resignFirstResponder];
    TIIDatePicker *picker = [[TIIDatePicker alloc] initWithDateFormatter:@"YYYY-MM-dd"];
    picker.delegate = self;
    picker.tag = 2;
    [picker show];
    return;
}

- (IBAction)chooseMsgType:(id)sender
{
    [self.tfKeyWords resignFirstResponder];
    CTPikcerView *controller = [[CTPikcerView alloc] initWithComponentNum:1 keyArray:[NSArray arrayWithObjects: MSG_TYPE_TODO, MSG_TYPE_WARNING, MSG_TYPE_REMIND, nil] valueArray:[NSArray arrayWithObjects: @"待办", @"预警", @"提醒", nil] delegate:self cancelButtonTitle:@"取消" confirmButtonTitle:@"确定"];
    [controller show];
    return;
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}


#pragma mark -
#pragma mark CTTableView DataSource & Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"CELL";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"TIISearchReultCell" owner:nil options:nil];
    UIView *view = [elements objectAtIndex:0];
    view.frame = CGRectMake(0, 0, ScreenWidth, view.frame.size.height);
    [cell addSubview:view];
    
    UILabel *labTitle = (UILabel *)[cell viewWithTag:1];
    UILabel *labContent = (UILabel *)[cell viewWithTag:2];
    UILabel *labDate = (UILabel *)[cell viewWithTag:3];
    UILabel *labFrom = (UILabel *)[cell viewWithTag:4];
    
    TIICommonMsg *curMsg = [self.array objectAtIndex:indexPath.row];
    labTitle.text = curMsg.msgTitle;
    labContent.text = curMsg.msgContent;
    labDate.text = curMsg.msgDate;
    labFrom.text = [NSString stringWithFormat:@"来自：%@", curMsg.msgFrom];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TIICommonMsg *curMsg = [self.array objectAtIndex:indexPath.row];
    TIIMsgDetailController *controller = [[TIIMsgDetailController alloc] initWithNibName:nil bundle:nil];
    controller.curSysPublic = curMsg;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark TIIDatePicker Delegate
- (void)tiiDatePicker:(TIIDatePicker *)datePicker didSelectDate:(NSString *)date
{
    if (datePicker.tag == 1) {
        self.condition.beginDate = [NSString stringWithFormat:@"%@ 00:00:00", date];
        self.labBeginDate.text = date;
    } else if (datePicker.tag == 2) {
        self.condition.endDate = [NSString stringWithFormat:@"%@ 00:00:00", date];
        self.labEndDate.text = date;
    }
    return;
}

#pragma mark CTPikcerView Delegate
- (void)ctPickerView:(CTPikcerView *)pickerView didClickedButtonOnIndex:(NSInteger)index
{
    if (index == 1) {
        NSInteger selectedIndex = [pickerView.pickerView selectedRowInComponent:0];
        self.labMsgType.text = [pickerView.valueArray objectAtIndex:selectedIndex];
        self.condition.msgType = [pickerView.keyArray objectAtIndex:selectedIndex];
    }
    return;
}

#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
