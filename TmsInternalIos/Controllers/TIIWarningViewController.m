//
//  TIIWarningViewController.m
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-21.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIIWarningViewController.h"
#import "CTTableView.h"
#import "TIIWebService.h"
#import "CTSoapConnection.h"
#import "TIICommonMsg.h"
#import "TIISqlLite.h"
#import "TIIMsgDetailController.h"

#define URL_TAG_LOADDATA 0
#define URL_TAG_UPDATEREADSTATUS 100

@interface TIIWarningViewController ()<UITableViewDataSource, UITableViewDelegate, CTTableViewDelegate, UIScrollViewDelegate, CTSoapConnectionDelegate>
@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSMutableArray *msgArray;

- (void)updateReadStatus:(NSString *)msgId withTag:(NSInteger)tag;
- (void)drawView;

@end

@implementation TIIWarningViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.msgArray = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        self.tv.pullUpViewEnabled = NO;
        [self.view addSubview:self.tv];
        [self loadData];
    } else {
        if ([TIIWebService unreadCount].unreadWarningCnt > 0) {
            [self loadData];
        }
    }
    return;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - soap request
- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在加载..";
    [self.indicator show];
    
    NSString *params = [TIIWebService getMsgByTypeParams:MSG_TYPE_WARNING];
    
    CTSoapConnection * conn = [[CTSoapConnection alloc] initWithFunctionName:WSDL_FUNCNAME_GETMSGBYTYPE params:params delegate:self];
    conn.tag = URL_TAG_LOADDATA;
    [conn start];
    return;
}

- (void)updateReadStatus:(NSString *)msgId withTag:(NSInteger)tag
{
    NSString *params = [TIIWebService updateReadStatusParams:msgId];
    
    CTSoapConnection * conn = [[CTSoapConnection alloc] initWithFunctionName:WSDL_FUNCNAME_UPDATEREADSTATUS params:params delegate:self];
    conn.tag = tag;
    [conn start];
    return;
}

- (void)drawView
{
    NSArray *allMsgArray = [[TIISqlLite shareManager] getMsgByType:MSG_TYPE_WARNING];
    self.msgArray  = [NSMutableArray arrayWithArray:allMsgArray];
    
    [self.tv reloadData];
    return;
}

#pragma mark - click event
- (void)warningExpandClicked:(UIControl *)control
{
    TIICommonMsg *curToDo = [self.msgArray objectAtIndex:control.tag];
//    self.WarningExpand = !self.WarningExpand;
    curToDo.expand = !curToDo.expand;
    NSIndexSet *set = [[NSIndexSet alloc] initWithIndex:control.tag];
    [self.tv reloadSections:set withRowAnimation:UITableViewRowAnimationFade];
    return;
}

- (void)focusClicked:(UIButton *)button
{
//    NSLog(@"focusClicked() tag=%d", button.tag);
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark - CTTableView Delegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    if (self.indicator.showing) {
        return NO;
    }
    [self loadData];
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    TIICommonMsg *curToDo = [self.msgArray objectAtIndex:section];
    if (curToDo.expand) {
        return 2;
    }
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.msgArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger height = 0;
    if (indexPath.row == 0) {
        height = 45;
    } else {
        height = 60;
    }
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"CELL";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    TIICommonMsg *item = [self.msgArray objectAtIndex:indexPath.section];
    if (indexPath.row == 0) {
        NSArray *header = [[NSBundle mainBundle] loadNibNamed:@"TIISystemPublicHeaderCell" owner:nil options:nil];
        UIView *headerView = [header objectAtIndex:0];
        CGRect headerViewFrame = headerView.frame;
        headerViewFrame.size.width = ScreenWidth;
        headerView.frame = headerViewFrame;
        
        UIControl *backControl = (UIControl *)[headerView viewWithTag:1];
        backControl.tag = indexPath.section;
        [backControl addTarget:self action:@selector(warningExpandClicked:) forControlEvents:UIControlEventTouchUpInside];
        UILabel  *titleLabel = (UILabel *)[headerView viewWithTag:10];
        titleLabel.text = item.msgTitle;
        
        UIImageView *ivLine = [[UIImageView alloc] init];
        ivLine.image = [[UIImage imageNamed:@"Xuxian"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        ivLine.frame = CGRectMake(15, 44, ScreenWidth - 30, 1);
        [headerView addSubview:ivLine];
        [cell addSubview:headerView];
    } else {
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"TIIWarningCell" owner:nil options:nil];
        UIView *view = [elements objectAtIndex:0];
        CGRect viewFrame = view.frame;
        viewFrame.size.width = ScreenWidth;
        view.frame = viewFrame;
        [cell addSubview:view];
        
        UILabel *labTitle = (UILabel *)[cell viewWithTag:1];
        UILabel *labDate = (UILabel *)[cell viewWithTag:2];
        UIButton *btnFocus = (UIButton *)[cell viewWithTag:3];
        
        labTitle.text = item.msgContent;
        labDate.text = item.msgDate;
        
        [labTitle sizeToFit];
        [btnFocus setBackgroundImage:[[UIImage imageNamed:@"Care"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 35, 5, 5)]  forState:UIControlStateNormal];
        [btnFocus setBackgroundImage:[[UIImage imageNamed:@"CareHighLIght"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 35, 5, 5)]  forState:UIControlStateHighlighted];
        [btnFocus addTarget:self action:@selector(focusClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self.delegate respondsToSelector:@selector(tiiWarningViewControllerDelegate:pushController:)]) {
        TIICommonMsg *curPublic = [self.msgArray objectAtIndex:indexPath.section];
        TIIMsgDetailController *tagController = [[TIIMsgDetailController alloc] init];
        tagController.curSysPublic = curPublic;
        tagController.title = @"提醒详情";
        [self.delegate tiiWarningViewControllerDelegate:self pushController:tagController];
    }
}

#pragma mark -
#pragma mark CTSoapConnection DataSource & Delegate
// 请求结束执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == URL_TAG_LOADDATA) {
        TIICommonMsg *warning = [[TIICommonMsg alloc] init];
        [warning parseData:data complete:^(NSArray *array) {
            [self.indicator hide];
            [self.msgArray removeAllObjects];
            // 存数据库
            [[TIISqlLite shareManager] addNewMsg:array];
            [self drawView];
            
            // 更改已阅状态
            for (int i = 0; i < array.count; i++) {
                TIICommonMsg *unreadMsg = [array objectAtIndex:i];
                [self updateReadStatus:unreadMsg.msgId withTag:URL_TAG_UPDATEREADSTATUS + i];
            }
            
            // 更改未读数量
            TIIUnreadCount *unreadCnt = [TIIWebService unreadCount];
            unreadCnt.unreadWarningCnt = 0;
            [TIIWebService setUnreadCount:unreadCnt];
            // 发送消息数量通知
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFCATION_MESSAGE_COUNT object:nil];
        }];
    } else if (connection.tag >= URL_TAG_UPDATEREADSTATUS) {
//        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//        NSLog(@"tag=%d result=%@", connection.tag, result);
        return;
    }
    return;
}

// 请求失败时执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    if (connection.tag == URL_TAG_LOADDATA) {
        [self drawView];
        return;
    }
    [self.tv reloadData];
    return;
}

@end
