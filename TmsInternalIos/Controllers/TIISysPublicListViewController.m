//
//  TIIMyCareListViewController.m
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-21.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIISysPublicListViewController.h"
#import "CTTableView.h"
#import "TIICommonMsg.h"
#import "TIIWebService.h"
#import "TIIMsgDetailController.h"
#import "CTSoapConnection.h"
#import "TIISqlLite.h"

#define URL_TAG_LOADSYSTEMPUBLIC 0
#define URL_TAG_UPDATEREADSTATUS 100

@interface TIISysPublicListViewController () <CTTableViewDelegate, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, CTSoapConnectionDelegate>

@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSMutableArray *array;

- (void)loadSystemPublicData;
- (void)updateReadStatus:(NSString *)msgId withTag:(NSInteger)tag;
- (void)drawView;

@end

@implementation TIISysPublicListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"最新公告";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.array = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.pullUpViewEnabled = NO;
        self.tv.ctTableViewDelegate = self;
        self.tv.dataSource = self;
        self.tv.delegate = self;
        [self.view addSubview:self.tv];
        
        [self loadSystemPublicData];
    }
    return;
}

#pragma mark -
#pragma mark url request
- (void)loadSystemPublicData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在加载..";
    [self.indicator show];
    
    NSString *params = [TIIWebService getMsgByTypeParams:MSG_TYPE_SYSPUB];
    
    CTSoapConnection * conn = [[CTSoapConnection alloc] initWithFunctionName:WSDL_FUNCNAME_GETMSGBYTYPE params:params delegate:self];
    conn.tag = URL_TAG_LOADSYSTEMPUBLIC;
    [conn start];
    return;
}

- (void)updateReadStatus:(NSString *)msgId withTag:(NSInteger)tag
{
    NSString *params = [TIIWebService updateReadStatusParams:msgId];
    
    CTSoapConnection * conn = [[CTSoapConnection alloc] initWithFunctionName:WSDL_FUNCNAME_UPDATEREADSTATUS params:params delegate:self];
    conn.tag = tag;
    [conn start];
    return;
}

- (void)drawView
{
    NSArray *allMsgArray = [[TIISqlLite shareManager] getMsgByType:MSG_TYPE_SYSPUB];
    self.array  = [NSMutableArray arrayWithArray:allMsgArray];
    
    [self.tv reloadData];
    return;
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark CTTableView DataSource & Delegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    if (self.indicator.showing) {
        return NO;
    }
    [self loadSystemPublicData];
    return YES;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger height = 0;
    if (indexPath.row == 0) {
        height = 45;
    } else {
        height = 77;
    }
    return height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.array.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"CELL";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor clearColor];
    
    if (indexPath.row == 0) {
        // header
        NSArray *header = [[NSBundle mainBundle] loadNibNamed:@"TIISystemPublicHeaderCell" owner:nil options:nil];
        UIView *headerView = [header objectAtIndex:0];
        
        UIImageView *ivLine = [[UIImageView alloc] init];
        ivLine.image = [[UIImage imageNamed:@"Xuxian"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        ivLine.frame = CGRectMake(15, 44, 290, 1);
        [headerView addSubview:ivLine];
        [cell addSubview:headerView];
        
        UIImageView *ivExpand = (UIImageView *)[cell viewWithTag:2];
        ivExpand.hidden = YES;
    } else {
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"TIISystemPublicCell" owner:nil options:nil];
        UIView *view = [elements objectAtIndex:0];
        [cell addSubview:view];
        
        UILabel *labTitle = (UILabel *)[cell viewWithTag:1];
        UILabel *labContent = (UILabel *)[cell viewWithTag:2];
        UILabel *labDate = (UILabel *)[cell viewWithTag:3];
        UILabel *labFrom = (UILabel *)[cell viewWithTag:4];
        
        TIICommonMsg *curItem = [self.array objectAtIndex:indexPath.row - 1];
        labTitle.text = curItem.msgTitle;
        labContent.text = curItem.msgContent;
        labDate.text = curItem.msgDate;
        labFrom.text = [NSString stringWithFormat:@"来自：%@", curItem.msgFrom];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TIICommonMsg *curPublic = [self.array objectAtIndex:indexPath.row - 1];
    TIIMsgDetailController *controller = [[TIIMsgDetailController alloc] initWithNibName:nil bundle:nil];
    controller.curSysPublic = curPublic;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -
#pragma mark CTSoapConnection DataSource & Delegate
// 请求结束执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == URL_TAG_LOADSYSTEMPUBLIC) {
        TIICommonMsg *public = [[TIICommonMsg alloc] init];
        [public parseData:data complete:^(NSArray *array) {
            [self.indicator hide];
            [self.array removeAllObjects];
            // 存数据库
            [[TIISqlLite shareManager] addNewMsg:array];
            [self drawView];
            
            // 更改已阅状态
            for (int i = 0; i < array.count; i++) {
                TIICommonMsg *unreadMsg = [array objectAtIndex:i];
                [self updateReadStatus:unreadMsg.msgId withTag:URL_TAG_UPDATEREADSTATUS + i];
            }
        }];
    } else if (connection.tag >= URL_TAG_UPDATEREADSTATUS) {
        //        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        //        NSLog(@"tag=%d result=%@", connection.tag, result);
        return;
    }
    return;
}

// 请求失败时执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    if (connection.tag == URL_TAG_LOADSYSTEMPUBLIC) {
        [self drawView];
        return;
    }
    [self.tv reloadData];
    return;
}

@end
