//
//  TIlLrpController.m
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-28.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIILrpController.h"
#import "CTTableView.h"
#import "CTAlertView.h"
#import "CTImageView.h"
#import "TIILrpMenu.h"
#import "TIIWebService.h"
#import "TIIWebViewController.h"

@interface TIILrpController () <CTTableViewDelegate, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, CTImageViewDelegate, CTAlertViewDelegate>

@property (nonatomic, strong) NSMutableArray * array;
@property (nonatomic, strong) CTTableView * tv;

- (void)loadData;

@end

@implementation TIILrpController


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.title = self.title;
    
    if (self.tv == nil) {
        self.array = [[NSMutableArray alloc] init];
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        self.tv.pullUpViewEnabled = NO;
        [self.view addSubview:self.tv];
        
        UIView * header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tv.frame.size.width, 10)];
        header.backgroundColor = [UIColor clearColor];
        [self.tv setTableHeaderView:header];
        [self loadData];
    }
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
}

- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    
    CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[TIIWebService lrpMenuWithTypeUrl:self.lrpType] delegate:self];
    [conn start];
    self.indicator.text = @"正在加载..";
    [self.indicator show];
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

#pragma mark -
#pragma mark CTTableView Delegate 
-(BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    if (self.indicator.showing) {
        return NO;
    } else {
        [self loadData];
        return YES;
    }
}

#pragma mark -
#pragma mark UITableView Delegate & DataSource 
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 105;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.array.count % 4 == 0) {
        return self.array.count / 4;
    } else {
        return self.array.count / 4 + 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"LRP_MENU_CELL";
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    for (int i = 0; i < 4; i++) {
        if (indexPath.row * 4 + i >= self.array.count) {
            break;
        }
        TIILrpMenu * item = [self.array objectAtIndex:(indexPath.row * 4 + i)];
        
        UIView * grid = [[UIView alloc] initWithFrame:CGRectMake(tableView.frame.size.width / 4 * i, 0, tableView.frame.size.width / 4, 80)];
        
        CTImageView * img = [[CTImageView alloc] initWithFrame:CGRectMake(15, 5, 50, 50)];
        img.url = item.imgUrl;
        img.imgId = [NSString stringWithFormat:@"%d", (indexPath.row * 4 + i)];
        img.delegate = self;
        [img loadImage];
        [grid addSubview:img];
        
        UILabel * name = [[UILabel alloc] initWithFrame:CGRectMake(0, 60, grid.frame.size.width, 45)];
        name.backgroundColor = [UIColor clearColor];
        name.font = [UIFont systemFontOfSize:14.0f];
        name.textAlignment = NSTextAlignmentCenter;
        name.text = item.appName;
        name.numberOfLines = 2;
        [grid addSubview:name];
        
        [cell addSubview:grid];
    }
    
    return cell;
    
}

#pragma mark -
#pragma mark CTImageView Delegate
- (void)ctImageViewDidClicked:(CTImageView *)ctImageView
{
    TIIWebViewController * controller = [[TIIWebViewController alloc] init];
    controller.menu = [self.array objectAtIndex:[ctImageView.imgId integerValue]];
    [self.navigationController presentViewController:controller animated:YES completion:nil];
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"请求失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    [self.tv reloadData];
    
    CTAlertView * alert = [[CTAlertView alloc] initWithTitle:@"系统提示" message:@"功能列表加载失败，是否重试？" delegate:self cancelButtonTitle:@"取消" confirmButtonTitle:@"重试"];
    [alert show];
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    TIILrpMenu * item = [[TIILrpMenu alloc] init];
    [item parseData:data complete:^(NSArray *array){
        [self.array removeAllObjects];
        [self.array addObjectsFromArray:array];
        [self.tv reloadData];
        [self.indicator hide];
    }];
}

#pragma mark -
#pragma mark CTAlertView Delegate
- (void)ctAlertView:(CTAlertView *)alertView didClickedButtonOnIndex:(NSInteger)index
{
    if (index == 1) {
        [self loadData];
    }
}

@end
