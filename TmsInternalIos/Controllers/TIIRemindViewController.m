//
//  TIIRemindViewController.m
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-21.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIIRemindViewController.h"
#import "CTTableView.h"
#import "TIIWebService.h"
#import "CTSoapConnection.h"
#import "TIICommonMsg.h"
#import "TIISqlLite.h"
#import "TIIMsgDetailController.h"

#define URL_TAG_LOADDATA 0
#define URL_TAG_UPDATEREADSTATUS 100

@interface TIIRemindViewController ()<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, CTTableViewDelegate, CTSoapConnectionDelegate>

@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSMutableArray *msgArray;

- (void)updateReadStatus:(NSString *)msgId withTag:(NSInteger)tag;
- (void)drawView;

@end

@implementation TIIRemindViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.msgArray = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.delegate = self;
        self.tv.dataSource = self;
        self.tv.ctTableViewDelegate = self;
        self.tv.pullUpViewEnabled = NO;
        [self.view addSubview:self.tv];
        
        [self loadData];
    } else {
        if ([TIIWebService unreadCount].unreadRemindCnt > 0) {
            [self loadData];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - soap request
- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在加载..";
    [self.indicator show];
    
    NSString *params = [TIIWebService getMsgByTypeParams:MSG_TYPE_REMIND];
    
    CTSoapConnection * conn = [[CTSoapConnection alloc] initWithFunctionName:WSDL_FUNCNAME_GETMSGBYTYPE params:params delegate:self];
    conn.tag = URL_TAG_LOADDATA;
    [conn start];
    return;
}

- (void)updateReadStatus:(NSString *)msgId withTag:(NSInteger)tag
{
    NSString *params = [TIIWebService updateReadStatusParams:msgId];
    
    CTSoapConnection * conn = [[CTSoapConnection alloc] initWithFunctionName:WSDL_FUNCNAME_UPDATEREADSTATUS params:params delegate:self];
    conn.tag = tag;
    [conn start];
    return;
}

- (void)drawView
{
    NSArray *allMsgArray = [[TIISqlLite shareManager] getMsgByType:MSG_TYPE_REMIND];
    self.msgArray  = [NSMutableArray arrayWithArray:allMsgArray];
    
    [self.tv reloadData];
    return;
}

#pragma mark - click event
- (void)focusClicked:(UIButton *)button
{
//    NSLog(@"focusClicked() tag=%d", button.tag);
}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark - CTTableView Delegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    if (self.indicator.showing) {
        return NO;
    }
    [self loadData];
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.msgArray.count;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60 + 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * identifier = @"CELL";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell ==  nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"TIIRemindCell" owner:nil options:nil];
        UIView *view = [elements objectAtIndex:0];
        view.frame = CGRectMake(5, 5, ScreenWidth - 10, view.frame.size.height);
        view.layer.borderWidth = 1;
        view.layer.borderColor = [[UIColor colorWithRed:225.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1] CGColor];
        [cell addSubview:view];
    }
    UILabel *labTitle = (UILabel *)[cell viewWithTag:1];
    UILabel *labDate = (UILabel *)[cell viewWithTag:2];
    UIButton *btnFocus = (UIButton *)[cell viewWithTag:3];

    
    TIICommonMsg *curRemind = [self.msgArray objectAtIndex:indexPath.row];
    labTitle.text = curRemind.msgContent;
    labDate.text = curRemind.msgDate;
    [btnFocus setBackgroundImage:[[UIImage imageNamed:@"Care"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 35, 5, 5)]  forState:UIControlStateNormal];
    [btnFocus setBackgroundImage:[[UIImage imageNamed:@"CareHighLIght"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 35, 5, 5)]  forState:UIControlStateHighlighted];
    [btnFocus addTarget:self action:@selector(focusClicked:) forControlEvents:UIControlEventTouchUpInside];
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self.delegate respondsToSelector:@selector(tiiRemindViewControllerDelegate:pushController:)]) {
        TIICommonMsg *curPublic = [self.msgArray objectAtIndex:indexPath.row];
        TIIMsgDetailController *tagController = [[TIIMsgDetailController alloc] init];
        tagController.curSysPublic = curPublic;
        tagController.title = @"提醒详情";
        [self.delegate tiiRemindViewControllerDelegate:self pushController:tagController];
    }
}

#pragma mark -
#pragma mark CTSoapConnection DataSource & Delegate
// 请求结束执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == URL_TAG_LOADDATA) {
        TIICommonMsg *remind = [[TIICommonMsg alloc] init];
        [remind parseData:data complete:^(NSArray *array) {
            [self.indicator hide];
            [self.msgArray removeAllObjects];
            // 存数据库
            [[TIISqlLite shareManager] addNewMsg:array];
            [self drawView];
            
            // 更改已阅状态
            for (int i = 0; i < array.count; i++) {
                TIICommonMsg *unreadMsg = [array objectAtIndex:i];
                [self updateReadStatus:unreadMsg.msgId withTag:URL_TAG_UPDATEREADSTATUS + i];
            }
            
            // 更改未读数量
            TIIUnreadCount *unreadCnt = [TIIWebService unreadCount];
            unreadCnt.unreadRemindCnt = 0;
            [TIIWebService setUnreadCount:unreadCnt];
            // 发送消息数量通知
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFCATION_MESSAGE_COUNT object:nil];
        }];
    } else if (connection.tag >= URL_TAG_UPDATEREADSTATUS) {
//        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//        NSLog(@"tag=%d result=%@", connection.tag, result);
        return;
    }
    return;
}

// 请求失败时执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    if (connection.tag == URL_TAG_LOADDATA) {
        [self drawView];
        return;
    }
    [self.tv reloadData];
    return;
}

@end
