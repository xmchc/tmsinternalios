//
//  TIIWebViewController.m
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-29.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIIWebViewController.h"
#import "TIILrpMenu.h"
#import "TIIWebService.h"
#import "CTAlertView.h"
#import "TIIWeekPicker.h"
#import "TIIDatePicker.h"
#import <QuartzCore/QuartzCore.h>

@interface TIIWebViewController () <UIWebViewDelegate, CTAlertViewDelegate, TIIWeekPickerDelegate, TIIDatePickerDelegate>

@property (nonatomic, strong) UIWebView * webView;
@property (nonatomic, strong) NSString * cancelJs;
@property (nonatomic, strong) NSString * confirmJs;
@property (nonatomic, strong) TIIWeekPicker * weekPicker;
@property (nonatomic, strong) CTIndicateView * toast;
@property (nonatomic, strong) UIButton * btnBack;

- (void)loading:(NSString *)method msg:(NSString *)msg;
- (NSString *)formatUrl:(NSString *)pramas;
- (void)dismiss;

@end

@implementation TIIWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImageView * bg = [[UIImageView alloc] initWithFrame:self.view.bounds];
    bg.image = [UIImage imageNamed:@"TIINavBg"];
    [self.view addSubview:bg];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.webView == nil){
        
        NSURLCache * cache = [NSURLCache sharedURLCache];
        [cache removeAllCachedResponses];
        [cache setDiskCapacity:0];
        [cache setMemoryCapacity:0];
        
        int y = 0;
        if ([[UIDevice currentDevice].systemVersion intValue] >= 7) {
            y = 20;
            
        }
        self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, y, self.view.bounds.size.width, self.view.bounds.size.height - y)];
        self.webView.delegate = self;
        self.webView.backgroundColor = [UIColor clearColor];
        self.webView.scalesPageToFit = YES;
        self.webView.dataDetectorTypes = UIDataDetectorTypeNone;
        
        for (UIView * subView in self.webView.subviews) {
            if ([subView isKindOfClass:[UIScrollView class]]) {
                ((UIScrollView *)subView).bounces = NO;
            }
        }
        [self.view addSubview:self.webView];
        
        
        
        BiPerson * person = [TIIWebService user];
        NSString * url = self.menu.appUrl;
        if ([[url lastPathComponent] rangeOfString:@"?"].location == NSNotFound) {
            url = [NSString stringWithFormat:@"%@?platform=ios&personCode=%@&sessionCode=%@&orgCode=%@", url, person.personCode, person.sessionCode, person.orgCode];
        } else {
            url = [NSString stringWithFormat:@"%@&platform=ios&personCode=%@&sessionCode=%@&orgCode=%@", url, person.personCode, person.sessionCode, person.orgCode];
        }
        // 判断issueId
        if (self.issueId) {
            url = [url stringByAppendingString:[NSString stringWithFormat:@"&issueId=%@",self.issueId]];
        }
        if (self.companyCode) {
            
            url = [TIIWebService widePriceShow:self.companyCode];
            
            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
            
        } else {
            
            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
            
            [self performSelector:@selector(loading) withObject:nil afterDelay:0.1];
        }
    }
    
    if (self.toast == nil) {
        self.toast = [[CTIndicateView alloc] initInView:self.view];
    }
    
    if (self.btnBack == nil) {
        self.btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.btnBack setImage:[UIImage imageWithContentsOfFile:HBResourcesBundle(@"NavBack.png")] forState:UIControlStateNormal];
        [self.btnBack setImage:[UIImage imageWithContentsOfFile:HBResourcesBundle(@"NavBack.png")] forState:UIControlStateHighlighted];
        self.btnBack.frame = CGRectMake(10, self.view.frame.size.height - 40 - 10, 40, 40);
        self.btnBack.backgroundColor = [UIColor colorWithRed:86.0/255.9 green:35.0/255.0 blue:94.0/255.0 alpha:0.5];
        self.btnBack.layer.cornerRadius = 20.0f;
        [self.btnBack addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        self.btnBack.showsTouchWhenHighlighted = YES;
        [self.view addSubview:self.btnBack];
    }
}

- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
    if (self.delegate && [self.delegate respondsToSelector:@selector(recvDiss:Bool:)]) {
        [self.delegate recvDiss:self Bool:YES];
    }
}

- (void)loading
{
    self.indicator.text = @"加载中";
    [self.indicator show];
}

- (void)loading:(NSString *)method msg:(NSString *)msg
{
    if ([method isEqualToString:@"show"]) {
        self.indicator.text = msg;
        
        if (!self.indicator.showing) {
            [self.indicator show];
        }
        
    } else {
        [self.indicator hide];
    }
}

- (NSString *)formatUrl:(NSString *)pramas
{
    NSString * str = [pramas stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([str hasPrefix:@"("]) {
        str = [str substringFromIndex:1];
    }
    if ([str hasSuffix:@")"]) {
        str = [str substringToIndex:str.length - 1];
    }
    return [[[str stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"'" withString:@""] stringByReplacingOccurrencesOfString:@"\"" withString:@""];
}

#pragma mark -
#pragma mark UIWebView Delegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString * lastCompent = request.URL.lastPathComponent;
//    NSLog(@"%@", lastCompent);
    
    //捕获退出
    if (self.companyCode) {
        if ([lastCompent isEqualToString:@"weChatBindGSLM.html"]) {
            [self dismissViewControllerAnimated:YES completion:nil];
            if (self.delegate && [self.delegate respondsToSelector:@selector(recvDiss:Bool:)]) {
                [self.delegate recvDiss:self Bool:YES];
            }
        }
    }
    
    if ([lastCompent hasPrefix:@"window.hsit."]) {
        lastCompent = [lastCompent stringByReplacingOccurrencesOfString:@"window.hsit." withString:@""];
        if ([lastCompent hasPrefix:@"back"]){
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        // 显示或隐藏加载指示器
        else if ([lastCompent hasPrefix:@"loading"]) {
            lastCompent = [lastCompent stringByReplacingOccurrencesOfString:@"loading" withString:@""];
            lastCompent = [self formatUrl:lastCompent];
            NSArray * array = [lastCompent componentsSeparatedByString:@","];
            if (array.count > 0) {
                NSString * method = [array objectAtIndex:0];
                NSString * msg = @"";
                if (array.count == 2) {
                    msg = [array objectAtIndex:1];
                }
                [self loading:method msg:msg];
            }
        }
        // 显示弹出框
        else if ([lastCompent hasPrefix:@"alert"]){
            lastCompent = [lastCompent stringByReplacingOccurrencesOfString:@"alert" withString:@""];
            lastCompent = [self formatUrl:lastCompent];
            NSArray * array = [lastCompent componentsSeparatedByString:@","];
            if (array.count == 4) {
                CTAlertView * alert = [[CTAlertView alloc] initWithTitle:[array objectAtIndex:0] message:[array objectAtIndex:1] delegate:self cancelButtonTitle:nil confirmButtonTitle:[array objectAtIndex:2]];
                self.confirmJs = [array objectAtIndex:3];
                alert.tag = 1;
                [alert show];
            } else if (array.count == 6){
                CTAlertView * alert = [[CTAlertView alloc] initWithTitle:[array objectAtIndex:0] message:[array objectAtIndex:1] delegate:self cancelButtonTitle:[array objectAtIndex:2] confirmButtonTitle:[array objectAtIndex:4]];
                self.cancelJs = [array objectAtIndex:3];
                self.confirmJs = [array objectAtIndex:5];
                alert.tag = 2;
                [alert show];
            }
        }
        // 选择年周
        else if ([lastCompent hasPrefix:@"weekPicker"]){
            lastCompent = [lastCompent stringByReplacingOccurrencesOfString:@"weekPicker" withString:@""];
            lastCompent = [self formatUrl:lastCompent];
            if (self.weekPicker == nil) {
                self.weekPicker = [[TIIWeekPicker alloc] init];
                self.weekPicker.delegate = self;
            }
            if (lastCompent.length == 6) {
                [self.weekPicker setSelectedYearWeek:lastCompent];
            }
            [self.weekPicker show];
        }
        // 选择日期
        else if ([lastCompent hasPrefix:@"datePicker"]){
            lastCompent = [lastCompent stringByReplacingOccurrencesOfString:@"datePicker" withString:@""];
            lastCompent = [self formatUrl:lastCompent];
            NSArray * array = [lastCompent componentsSeparatedByString:@","];
            if (array.count == 2) {
                TIIDatePicker * picker = [[TIIDatePicker alloc] initWithDateFormatter:[array objectAtIndex:0]];
                picker.delegate = self;
                [picker setCurrentDate:[array objectAtIndex:1]];
                [picker show];
            }
            
        }
        // 提示下就消失
        else if ([lastCompent hasPrefix:@"toast"]){
            lastCompent = [lastCompent stringByReplacingOccurrencesOfString:@"toast" withString:@""];
            lastCompent = [self formatUrl:lastCompent];
            if (!self.toast.showing) {
                self.toast.text = lastCompent;
                [self.toast autoHide:CTIndicateStateWarning afterDelay:1.0];
            }
        }
        return NO;
    } else {
        return YES;
    }
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    CTAlertView * alert = [[CTAlertView alloc] initWithTitle:@"系统提示" message:@"页面加载失败，请稍后再试！" delegate:self cancelButtonTitle:nil confirmButtonTitle:@"退出"];
    alert.tag = 3;
    [alert show];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    self.btnBack.hidden = YES;
}

#pragma mark -
#pragma mark CTAlertView Delegate
- (void)ctAlertView:(CTAlertView *)alertView didClickedButtonOnIndex:(NSInteger)index
{
    if (alertView.tag == 1) {
        if (self.confirmJs != nil && self.confirmJs.length != 0) {
            [self.webView stringByEvaluatingJavaScriptFromString:self.confirmJs];
        }
    } else if (alertView.tag == 2){
        if (index == 0) {
            if (self.cancelJs != nil && self.cancelJs.length != 0) {
                [self.webView stringByEvaluatingJavaScriptFromString:self.cancelJs];
            }
        } else {
            if (self.confirmJs != nil && self.confirmJs.length != 0) {
                [self.webView stringByEvaluatingJavaScriptFromString:self.confirmJs];
            }
        }
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark -
#pragma mark TIIWeekPicker Delegate
- (void)tiiWeekPickerDidSelected:(TIIWeekPicker *)weekPicker
{
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"onSelectedWeek('%@')", weekPicker.selectedYearWeekWithW]];
}

#pragma mark -
#pragma mark TIIDatePicker Delegate
- (void)tiiDatePicker:(TIIDatePicker *)datePicker didSelectDate:(NSString *)date
{
    [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"onSelectedDate('%@')", date]];
}

@end
