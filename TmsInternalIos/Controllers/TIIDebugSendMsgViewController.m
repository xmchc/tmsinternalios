//
//  TIIDebugSendMsgViewController.m
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-24.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIIDebugSendMsgViewController.h"
#import "TIIWebService.h"
#import "CTSoapConnection.h"
#import "TIICommonMsg.h"
#import "TIISqlLite.h"

@interface TIIDebugSendMsgViewController () <UITextFieldDelegate, CTSoapConnectionDelegate>

@property (nonatomic, strong) IBOutlet UITextField *tfID;
@property (nonatomic, strong) IBOutlet UITextField *tfTitle;
@property (nonatomic, strong) IBOutlet UITextField *tfContent;
@property (nonatomic, strong) IBOutlet UITextField *tfMsgType;

- (IBAction)btnSendClicked:(UIButton *)btn;
- (IBAction)btnAddClicked:(UIButton *)btn;

@end

@implementation TIIDebugSendMsgViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"发送消息";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)btnSendClicked:(UIButton *)btn
{
    NSDate *nowDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *destDateString = [dateFormatter stringFromDate:nowDate];
    
    NSString *xmlStr = [NSString stringWithFormat:
                        @"&lt;MESEntity&gt; "
                        
                        "&lt;MSG_ID&gt;%@&lt;/MSG_ID&gt; "
                        "&lt;BUS_ID&gt;CSAP100001&lt;/BUS_ID&gt; "
                        "&lt;MSG_TYPE&gt;%@&lt;/MSG_TYPE&gt; "
                        "&lt;HANDLE_TYPE&gt;1&lt;/HANDLE_TYPE&gt; "
                        "&lt;SEND_TYPE&gt;1&lt;/SEND_TYPE&gt; "
                        "&lt;LEVEL_TYPE&gt;0&lt;/LEVEL_TYPE&gt; "
                        "&lt;OBJ_TYPE&gt;0&lt;/OBJ_TYPE&gt; "
                        "&lt;PORT_TYPE&gt;HTTP&lt;/PORT_TYPE&gt; "
                        "&lt;REC_TER&gt;3&lt;/REC_TER&gt; "
                        "&lt;ORG_CODE&gt;13500200&lt;/ORG_CODE&gt; "
                        
                        "&lt;SYS_CODE&gt;107&lt;/SYS_CODE&gt; "
                        "&lt;MODEL_CODE&gt;16&lt;/MODEL_CODE&gt; "
                        "&lt;PROCESS_ID&gt;100001&lt;/PROCESS_ID&gt; "
                        
                        "&lt;SEND_PERSON&gt;1000&lt;/SEND_PERSON&gt; "
                        "&lt;SEND_DEPT&gt;135001000300&lt;/SEND_DEPT&gt; "
                        "&lt;SEND_DUTY&gt;GWDMA&lt;/SEND_DUTY&gt; "
                        "&lt;SEND_DATE&gt;%@&lt;/SEND_DATE&gt; "
                        "&lt;LAST_DATE&gt;2014-02-18 15:11:24&lt;/LAST_DATE&gt; "
                        
                        "&lt;MSG_TITLE&gt;%@&lt;/MSG_TITLE&gt; "
                        "&lt;MSG_CONTENT&gt;%@&lt;/MSG_CONTENT&gt; "
                        "&lt;MSG_URL&gt;/csap/jsp/archives/archivesManagement1.jsp&lt;/MSG_URL&gt; "
                        "&lt;MSG_URL_PHONE&gt;PHONE_resouce1&lt;/MSG_URL_PHONE&gt; "
                        "&lt;MSG_URL_PAD&gt;PAD_resouce1&lt;/MSG_URL_PAD&gt; "
                        
                        "&lt;MES_REC_LIST&gt; "
                        "&lt;MES_REC&gt; "
                        "&lt;REC_PERSON&gt;%@&lt;/REC_PERSON&gt; "
                        "&lt;REC_DEPT&gt;135001020305&lt;/REC_DEPT&gt; "
                        "&lt;REC_DUTY&gt;GWDMZ&lt;/REC_DUTY&gt; "
                        "&lt;/MES_REC&gt; "
                        "&lt;/MES_REC_LIST&gt; "
                        
                        "&lt;MES_ANNEX_LIST&gt; "
                        "&lt;/MES_ANNEX_LIST&gt; "
                        "&lt;/MESEntity&gt; "
                        , @"", self.tfMsgType.text, destDateString, self.tfTitle.text, self.tfContent.text, [TIIWebService user].personCode
                        ];
    
    [self sendMsg:xmlStr];
    return;
}

- (IBAction)btnAddClicked:(UIButton *)btn {
    NSDate *nowDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *destDateString = [dateFormatter stringFromDate:nowDate];
    
    TIICommonMsg *msg = [[TIICommonMsg alloc] init];
    msg.msgId = destDateString;
    msg.MSG_TYPE = self.tfMsgType.text;
    msg.msgDate = destDateString;
    msg.msgTitle = self.tfTitle.text;
    msg.msgContent = self.tfContent.text;
    msg.SEND_DATE = destDateString;
    // 存数据库
    [[TIISqlLite shareManager] addNewMsg:@[msg]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - soap request
- (void)sendMsg:(NSString *)xmlStr
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在发送..";
    [self.indicator show];
    
    NSString *params = [NSString stringWithFormat:
                        @"<xmlString>%@</xmlString>\n"
                        , xmlStr
                        ];
    
    CTSoapConnection * conn = [[CTSoapConnection alloc] initWithFunctionName:@"sendMsgToMQByXml" params:params delegate:self];
    [conn start];
    return;
}

#pragma mark -
#pragma mark CTSoapConnection DataSource & Delegate
// 请求结束执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFinishLoading:(NSData *)data
{
    NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    self.indicator.text = result;
    [self.indicator autoHide:CTIndicateStateDone afterDelay:1.2];
    return;
}

// 请求失败时执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    return;
}

@end
