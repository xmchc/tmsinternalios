//
//  TIIMainController.m
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-28.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIIMainController.h"
#import "TIILrpController.h"
#import "TIIMsgController.h"
#import "TIIWebService.h"
#import "TIILoginController.h"
#import "TIITaApp.h"
#import "TIIAppDelegate.h"
#import "TIIDeviceToken.h"
#import "CTSoapConnection.h"

#define URL_TAG_APPMODULE 0
#define URL_TAG_UPTOKEN 1

@interface TIIMainController () <CTURLConnectionDelegate, CTSoapConnectionDelegate>

@property (nonatomic, strong) NSMutableArray *array;
@property (nonatomic, strong) NSMutableArray *numArray;
@property (nonatomic, strong) NSMutableArray *controllerArray;   

@end

@implementation TIIMainController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.array = [[NSMutableArray alloc] init];
    self.numArray = [[NSMutableArray alloc] init];
    self.controllerArray = [[NSMutableArray alloc] init];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[TIIWebService taAppModule] delegate:self];
    conn.tag = URL_TAG_APPMODULE;
    [conn start];
    self.view.frame = self.navigationController.view.bounds;
    
    UIButton *btnback = [UIButton buttonWithType:UIButtonTypeCustom];
    btnback.frame = CGRectMake(0, 0, 22, 22);
    [btnback setImage:[UIImage imageNamed:@"NavBack"] forState:UIControlStateNormal];
    [btnback addTarget:self action:@selector(btnbackClicked:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftbtn = [[UIBarButtonItem alloc] initWithCustomView:btnback];
    self.navigationItem.leftBarButtonItem = leftbtn;
    
    if ([TIIDeviceToken sharedManager].deviceToken.length > 0) {
        [self upDeviceToken:[TIIDeviceToken sharedManager].deviceToken];
        return;
    }
}

- (void)btnbackClicked:(UIButton *)btn
{
    TIIAppDelegate *appDelegate = (TIIAppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate.controller logOut];
}

- (void)creatTabitem
{
    TIILrpController *applicationController = [[TIILrpController alloc] init];
    applicationController.lrpType = 0;
    [applicationController setTabBarItemWithTitle:@"应用" titleColor:[UIColor colorWithRed:153.0/255.0 green:88.0/255.0 blue:163.0/255.0 alpha:1] titleSelectedColor:[UIColor whiteColor] image:[UIImage imageNamed:@"TIITabCity" ] selectedImage:[UIImage imageNamed:@"TIITabCityHighlighted"]];
    
    TIILrpController * cityController = [[TIILrpController alloc] init];
    cityController.lrpType = 1;
    [cityController setTabBarItemWithTitle:@"全市" titleColor:[UIColor colorWithRed:153.0/255.0 green:88.0/255.0 blue:163.0/255.0 alpha:1] titleSelectedColor:[UIColor whiteColor] image:[UIImage imageNamed:@"TIITabCity"] selectedImage:[UIImage imageNamed:@"TIITabCityHighlighted"]];
    
    TIILrpController * provinceController = [[TIILrpController alloc] init];
    provinceController.lrpType = 2;
    [provinceController setTabBarItemWithTitle:@"全省" titleColor:[UIColor colorWithRed:153.0/255.0 green:88.0/255.0 blue:163.0/255.0 alpha:1] titleSelectedColor:[UIColor whiteColor] image:[UIImage imageNamed:@"TIITabProvince"] selectedImage:[UIImage imageNamed:@"TIITabProvinceHighlighted"]];
    
    TIILrpController * transactionController = [[TIILrpController alloc] init];
    transactionController.lrpType = 3;
    [transactionController setTabBarItemWithTitle:@"事务" titleColor:[UIColor colorWithRed:153.0/255.0 green:88.0/255.0 blue:163.0/255.0 alpha:1] titleSelectedColor:[UIColor whiteColor] image:[UIImage imageNamed:@"TIIEvent"] selectedImage:[UIImage imageNamed:@"TIIEventHighlighted"]];
    
    TIIMsgController * msgController = [[TIIMsgController alloc] initWithNibName:@"TIIMsgController" bundle:nil];
    
    
    for (int i = 0; i < self.array.count; i ++) {
        TIITaApp *item = [self.array objectAtIndex:i];
        [self.numArray addObject:item];
    }
    self.numArray = [NSMutableArray arrayWithArray:[self.numArray sortedArrayUsingFunction:sortType context:nil]];
    for (TIITaApp *item in self.numArray) {
        if ([item.appType isEqualToString:@"0"]) {
            [self.controllerArray addObject:applicationController];
        } else if ([item.appType isEqualToString:@"1"]){
            [self.controllerArray addObject:cityController];
        } else if ([item.appType isEqualToString:@"2"]){
            [self.controllerArray addObject:provinceController];
        } else if ([item.appType isEqualToString:@"3"]){
            [self.controllerArray addObject:transactionController];
        }
    }
//    if ([self.numArray containsObject:@"0"]) {
//        [self.controllerArray addObject:applicationController];
//    }
//    if ([self.numArray containsObject:@"1"]){
//        [self.controllerArray addObject:cityController];
//    }
//    if ([self.numArray containsObject:@"2"]){
//        [self.controllerArray addObject:provinceController];
//    }
//    if ([self.numArray containsObject:@"3"]) {
//        [self.controllerArray addObject:transactionController];
//    }
    [self.controllerArray addObject:msgController];
    self.viewControllers = [NSArray arrayWithArray:self.controllerArray];
}

NSInteger sortType(id s1,id s2,void *cha)
{
    TIITaApp *sx1 = (TIITaApp *)s1;
    TIITaApp *sx2 = (TIITaApp *)s2;
    
    
    if(sx1.appType.intValue > sx2.appType.intValue)
    {
        return NSOrderedDescending;
    }else if(sx1.appType.intValue < sx2.appType.intValue)
    {
        return NSOrderedAscending;
    }
    
    return NSOrderedSame;
}

- (void)upDeviceToken:(NSString *)token
{
    NSLog(@"upDeviceToken(token=%@)", token);
    NSString *params = [TIIWebService upTokenParams:token];
    
    CTSoapConnection * conn = [[CTSoapConnection alloc] initWithFunctionName:WSDL_FUNCNAME_UPDATEDEVICETOKEN params:params delegate:self];
    conn.tag = URL_TAG_UPTOKEN;
    [conn start];
    return;
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    TIITaApp *item = [[TIITaApp alloc] init];
    [item parseData:data complete:^(NSArray *array){
        [self.array removeAllObjects];
        [self.array addObjectsFromArray:array];
        [self creatTabitem];
    }];
}

#pragma mark - CTSoapConnection DataSource & Delegate
// 请求结束执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFinishLoading:(NSData *)data
{
    NSLog(@"upToken result=%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    return;
}

// 请求失败时执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"upToken failed!");
}

@end
