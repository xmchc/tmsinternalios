//
//  TIIMyCareViewController.m
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-21.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIIMyCareViewController.h"
#import "TIICommonMsg.h"
#import "CTTableView.h"
#import "TIIWebService.h"
#import "CTSoapConnection.h"
#import "TIISqlLite.h"
#import "TIISysPublicListViewController.h"
#import "TIIMsgDetailController.h"

#define URL_TAG_LOADSYSTEMPUBLIC 0
#define URL_TAG_LOADMYCARE 1
#define URL_TAG_UPDATEREADSTATUS 100

@interface TIIMyCareViewController () <CTTableViewDelegate, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, CTSoapConnectionDelegate>

@property (nonatomic, strong) CTTableView * tv;
@property (nonatomic, strong) NSMutableArray * systemPublicData;
@property (nonatomic, strong) NSMutableArray * myCareData;
@property (nonatomic) BOOL systemPublicExpand;
@property (nonatomic) BOOL myCareExpand;

- (void)loadSystemPublicData;
- (void)updateReadStatus:(NSString *)msgId withTag:(NSInteger)tag;
- (void)drawView;

- (void)systemPublicExpandClicked;
- (void)systemPublicMoreClicked;
- (void)mycareExpandClicked;
- (void)btnCancelCareClicked:(UIButton *)button;

@end

@implementation TIIMyCareViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.systemPublicData = [[NSMutableArray alloc] init];
    self.myCareData = [[NSMutableArray alloc] init];
    self.systemPublicExpand = YES;
    self.myCareExpand = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.pullUpViewEnabled = NO;
        self.tv.ctTableViewDelegate = self;
        self.tv.dataSource = self;
        self.tv.delegate = self;
        [self.view addSubview:self.tv];
        
        [self loadSystemPublicData];
    } else {
        if ([TIIWebService unreadCount].unreadSysPublicCnt > 0) {
            [self loadSystemPublicData];
        }
    }
}

#pragma mark -
#pragma mark click event
- (void)systemPublicExpandClicked
{
    self.systemPublicExpand = !self.systemPublicExpand;
    NSIndexSet *set = [[NSIndexSet alloc] initWithIndex:0];
    [self.tv reloadSections:set withRowAnimation:UITableViewRowAnimationFade];
    return;
}

- (void)systemPublicMoreClicked
{
    if ([self.delegate respondsToSelector:@selector(tiiMyCareViewControllerDelegate:pushController:)]) {
        TIISysPublicListViewController *tagController = [[TIISysPublicListViewController alloc] init];
        [self.delegate tiiMyCareViewControllerDelegate:self pushController:tagController];
    }
    return;
}

- (void)mycareExpandClicked
{
    self.myCareExpand = !self.myCareExpand;
    NSIndexSet *set = [[NSIndexSet alloc] initWithIndex:1];
    [self.tv reloadSections:set withRowAnimation:UITableViewRowAnimationFade];
    return;
}

- (void)btnCancelCareClicked:(UIButton *)button
{
    return;
}

#pragma mark -
#pragma mark url request
- (void)loadSystemPublicData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在加载..";
    [self.indicator show];
    
    NSString *params = [TIIWebService getMsgByTypeParams:MSG_TYPE_SYSPUB];
    
    CTSoapConnection * conn = [[CTSoapConnection alloc] initWithFunctionName:WSDL_FUNCNAME_GETMSGBYTYPE params:params delegate:self];
    conn.tag = URL_TAG_LOADSYSTEMPUBLIC;
    [conn start];
    return;
}

- (void)updateReadStatus:(NSString *)msgId withTag:(NSInteger)tag
{
    NSString *params = [TIIWebService updateReadStatusParams:msgId];
    
    CTSoapConnection * conn = [[CTSoapConnection alloc] initWithFunctionName:WSDL_FUNCNAME_UPDATEREADSTATUS params:params delegate:self];
    conn.tag = tag;
    [conn start];
    return;
}

- (void)drawView
{
    NSArray *allMsgArray = [[TIISqlLite shareManager] getMsgByType:MSG_TYPE_SYSPUB];
    if (allMsgArray.count > 0) {
        [self.systemPublicData addObject:[allMsgArray objectAtIndex:0]];
    }
    if (allMsgArray.count > 1) {
        [self.systemPublicData addObject:[allMsgArray objectAtIndex:1]];
    }
    
    [self.tv reloadData];
    return;
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark CTTableView DataSource & Delegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    if (self.indicator.showing) {
        return NO;
    }
    [self loadSystemPublicData];
    return YES;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger height = 0;
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            height = 45;
        } else if (indexPath.row == self.systemPublicData.count + 1) {
            height = 40;
        } else {
            height = 77;
        }
    } else {
        if (indexPath.row == 0) {
            height = 45;
        } else {
            height = 160;
        }
    }
    return height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger cnt = 0;
    if (section == 0) {
        if (self.systemPublicExpand) {
            cnt =  self.systemPublicData.count + 2;
        } else {
            cnt = 1;
        }
    } else {
        if (self.myCareExpand) {
            cnt = self.myCareData.count + 1;
        } else {
            cnt = 1;
        }
    }
    return cnt;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"CELL";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.section == 0) {
        // 系统公告
        if (indexPath.row == 0) {
            // header
            NSArray *header = [[NSBundle mainBundle] loadNibNamed:@"TIISystemPublicHeaderCell" owner:nil options:nil];
            UIView *headerView = [header objectAtIndex:0];
            
            UIControl *backControl = (UIControl *)[headerView viewWithTag:1];
            [backControl addTarget:self action:@selector(systemPublicExpandClicked) forControlEvents:UIControlEventTouchUpInside];
            
            UIImageView *ivLine = [[UIImageView alloc] init];
            ivLine.image = [[UIImage imageNamed:@"Xuxian"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
            ivLine.frame = CGRectMake(15, 44, 290, 1);
            [headerView addSubview:ivLine];
            [cell addSubview:headerView];
        } else if (indexPath.row == self.systemPublicData.count + 1) {
            NSArray *footer = [[NSBundle mainBundle] loadNibNamed:@"TIISystemPublicFooterCell" owner:nil options:nil];
            UIView *footerView = [footer objectAtIndex:0];
            
            UIControl *backControl = (UIControl *)[footerView viewWithTag:1];
            [backControl addTarget:self action:@selector(systemPublicMoreClicked) forControlEvents:UIControlEventTouchUpInside];
            
            [cell addSubview:footerView];
        } else {
            NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"TIISystemPublicCell" owner:nil options:nil];
            UIView *view = [elements objectAtIndex:0];
            [cell addSubview:view];
            
            UILabel *labTitle = (UILabel *)[cell viewWithTag:1];
            UILabel *labContent = (UILabel *)[cell viewWithTag:2];
            UILabel *labDate = (UILabel *)[cell viewWithTag:3];
            UILabel *labFrom = (UILabel *)[cell viewWithTag:4];
            
            TIICommonMsg *curItem = [self.systemPublicData objectAtIndex:indexPath.row - 1];
            labTitle.text = curItem.msgTitle;
            labContent.text = curItem.msgContent;
            labDate.text = curItem.msgDate;
            labFrom.text = [NSString stringWithFormat:@"来自：%@", curItem.msgFrom];
        }
    } else {
        // 我的关注
        if (indexPath.row == 0) {
            // header
            NSArray *header =  [[NSBundle mainBundle] loadNibNamed:@"TIIMyCareHeaderCell" owner:nil options:nil];
            UIView *headerView = [header objectAtIndex:0];
            
            UIControl *backControl = (UIControl *)[headerView viewWithTag:1];
            [backControl addTarget:self action:@selector(mycareExpandClicked) forControlEvents:UIControlEventTouchUpInside];
            
            UIImageView *ivLine = [[UIImageView alloc] init];
            ivLine.image = [[UIImage imageNamed:@"Xuxian"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
            ivLine.frame = CGRectMake(15, 44, 290, 1);
            [headerView addSubview:ivLine];
            [cell addSubview:headerView];
        } else {
            NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"TIIMyCareCell" owner:nil options:nil];
            UIView *view = [elements objectAtIndex:0];
            [cell addSubview:view];
            
            UILabel *labTitle = (UILabel *)[cell viewWithTag:1];
            UILabel *labContent = (UILabel *)[cell viewWithTag:2];
            UILabel *labDate = (UILabel *)[cell viewWithTag:3];
            UIButton *btnCancelCare = (UIButton *)[cell viewWithTag:4];
            
            TIICommonMsg *curItem = [self.myCareData objectAtIndex:indexPath.row - 1];
            labTitle.text = curItem.msgTitle;
            labContent.text = curItem.msgContent;
            labDate.text = curItem.msgDate;
            UIImage *cancelCare = [[UIImage imageNamed:@"CancelCare"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 22, 3, 3)];
            UIImage *cancelCareHightLight = [[UIImage imageNamed:@"CancelCareHighLight"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 22, 3, 3)];
            [btnCancelCare setBackgroundImage:cancelCare forState:UIControlStateNormal];
            [btnCancelCare setBackgroundImage:cancelCareHightLight forState:UIControlStateHighlighted];
            [btnCancelCare addTarget:self action:@selector(btnCancelCareClicked:) forControlEvents:UIControlEventTouchUpInside];
            btnCancelCare.tag = indexPath.row - 1;
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        // 系统公告
        if (indexPath.row == 0) {
            // header
        } else if (indexPath.row == self.systemPublicData.count + 1) {
            // footer
        } else {
            if ([self.delegate respondsToSelector:@selector(tiiMyCareViewControllerDelegate:pushController:)]) {
                TIICommonMsg *curPublic = [self.systemPublicData objectAtIndex:indexPath.row - 1];
                TIIMsgDetailController *tagController = [[TIIMsgDetailController alloc] initWithNibName:nil bundle:nil];
                tagController.curSysPublic = curPublic;
                tagController.title = @"关注详情";
                [self.delegate tiiMyCareViewControllerDelegate:self pushController:tagController];
            }
        }
    }
}

#pragma mark -
#pragma mark CTSoapConnection DataSource & Delegate
// 请求结束执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == URL_TAG_LOADSYSTEMPUBLIC) {
        TIICommonMsg *public = [[TIICommonMsg alloc] init];
        [public parseData:data complete:^(NSArray *array) {
            [self.indicator hide];
            [self.systemPublicData removeAllObjects];
            // 存数据库
            [[TIISqlLite shareManager] addNewMsg:array];
            [self drawView];
            
            // 更改已阅状态
            for (int i = 0; i < array.count; i++) {
                TIICommonMsg *unreadMsg = [array objectAtIndex:i];
                [self updateReadStatus:unreadMsg.msgId withTag:URL_TAG_UPDATEREADSTATUS + i];
            }
            
            // 更改未读数量
            TIIUnreadCount *unreadCnt = [TIIWebService unreadCount];
            unreadCnt.unreadSysPublicCnt = 0;
            [TIIWebService setUnreadCount:unreadCnt];
            // 发送消息数量通知
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFCATION_MESSAGE_COUNT object:nil];
        }];
    } else if (connection.tag >= URL_TAG_UPDATEREADSTATUS) {
//        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//        NSLog(@"tag=%d result=%@", connection.tag, result);
        return;
    }
    return;
}

// 请求失败时执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    if (connection.tag == URL_TAG_LOADSYSTEMPUBLIC) {
        [self drawView];
        return;
    }
    [self.tv reloadData];
    return;
}

@end
