//
//  ConsMsgDetailController.m
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-3.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//


#import "TIIMsgDetailController.h"

@interface TIIMsgDetailController ()

@property (nonatomic, strong) UILabel * titleLabel;        // 详情标题
@property (nonatomic, strong) UILabel * publishTimeLabel;  // 时间
@property (nonatomic, strong) UIWebView * contentView;     // 内容视图

@end

@implementation TIIMsgDetailController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.titleLabel == nil) {
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.view.bounds.size.width - 10, 60)];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.numberOfLines = 2;
        [self.view addSubview:self.titleLabel];
        
        self.publishTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        self.publishTimeLabel.font = [UIFont systemFontOfSize:10.0f];
        self.publishTimeLabel.textColor = [UIColor lightGrayColor];
        self.publishTimeLabel.numberOfLines = 1;
        self.publishTimeLabel.backgroundColor = [UIColor clearColor];
        self.publishTimeLabel.text = @"2000-01-01 00:00:00";
        [self.publishTimeLabel sizeToFit];
        self.publishTimeLabel.center = CGPointMake(self.view.bounds.size.width - self.publishTimeLabel.frame.size.width / 2 - 10, 60 + self.publishTimeLabel.frame.size.height / 2);
        [self.view addSubview:self.publishTimeLabel];
        
        UIImageView *dotLine = [[UIImageView alloc] initWithFrame:CGRectMake(5, self.publishTimeLabel.frame.origin.y + self.publishTimeLabel.frame.size.height + 5, self.view.bounds.size.width - 10, 1)];
        dotLine.image = [[UIImage imageNamed:@"Xuxian"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [self.view addSubview:dotLine];
        
        self.contentView = [[UIWebView alloc] initWithFrame:CGRectMake(5, dotLine.frame.origin.y + dotLine.frame.size.height + 5, self.view.bounds.size.width - 10, self.view.bounds.size.height - dotLine.frame.size.height - dotLine.frame.origin.y - 5 - 5)];
        self.contentView.backgroundColor = [UIColor clearColor];
        self.contentView.dataDetectorTypes = UIDataDetectorTypeNone;
        self.contentView.opaque = NO;
        [[[self.contentView subviews] objectAtIndex:0] setBounces:NO];
        [self.view addSubview:self.contentView];
    }
    
    self.titleLabel.text = self.curSysPublic.msgTitle;
    self.publishTimeLabel.text = [NSString stringWithFormat:@"%@   来自：%@", self.curSysPublic.msgDate, self.curSysPublic.msgFrom];
    [self.publishTimeLabel sizeToFit];
    self.publishTimeLabel.center = CGPointMake(self.view.bounds.size.width - self.publishTimeLabel.frame.size.width / 2 - 10, 60 + self.publishTimeLabel.frame.size.height / 2);
    [self.view addSubview:self.publishTimeLabel];
    [self showOnSaleActContent];
    
}

- (void)showOnSaleActContent
{
    NSString * cont = self.curSysPublic.msgContent;
    cont = [cont stringByReplacingOccurrencesOfString:@"        " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"       " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"      " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"     " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@"    " withString:@"</br>"];
    cont = [cont stringByReplacingOccurrencesOfString:@" " withString:@"</br>"];
    
    NSMutableString * imgString = [[NSMutableString alloc] init];
//    if (self.onSaleAct.msgImg01.length != 0 && ![self.onSaleAct.msgImg01 isEqualToString:@"/"]) {
//        [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [ConsWebService fileFullUrl:self.onSaleAct.msgImg01], self.view.bounds.size.width - 20]];
//    }
//    if (self.onSaleAct.msgImg02.length != 0 && ![self.onSaleAct.msgImg02 isEqualToString:@"/"]) {
//        [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [ConsWebService fileFullUrl:self.onSaleAct.msgImg02], self.view.bounds.size.width - 20]];
//    }
//    if (self.onSaleAct.msgImg03.length != 0 && ![self.onSaleAct.msgImg03 isEqualToString:@"/"]) {
//        [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [ConsWebService fileFullUrl:self.onSaleAct.msgImg03], self.view.bounds.size.width - 20]];
//    }
//    if (self.onSaleAct.msgImg04.length != 0 && ![self.onSaleAct.msgImg04 isEqualToString:@"/"]) {
//        [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [ConsWebService fileFullUrl:self.onSaleAct.msgImg04], self.view.bounds.size.width - 20]];
//    }
//    if (self.onSaleAct.msgImg05.length != 0 && ![self.onSaleAct.msgImg05 isEqualToString:@"/"]) {
//        [imgString appendString:[NSString stringWithFormat:@"<img src='%@' width=%.0f style='margin-bottom:3px'/></br>", [ConsWebService fileFullUrl:self.onSaleAct.msgImg05], self.view.bounds.size.width - 20]];
//    }
    
    
    
    [self.contentView loadHTMLString:[NSString stringWithFormat:@"%@</br><span style='font-size:14px; color:#3D3D3D;'>%@</span></br></br>", imgString, cont] baseURL:nil];
    return;
}

@end
