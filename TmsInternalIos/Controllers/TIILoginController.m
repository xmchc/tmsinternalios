//
//  TIILoginController.m
//  TmsInternalIos
//
//  Created by 宏超 陈 on 14-5-28.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIILoginController.h"
#import "TIIWebService.h"
#import "SystemMsg.h"
#import "TIILoginInfo.h"
#import "TIISqlLite.h"
#import "TIIOrgPicker.h"
#import "TIIWebViewController.h"

#define GET_LOGININFO 1
#define GET_VERIFYLOGIN  2
#define GET_PICKDATA 3
#define get_GYVERIFY 4
#define LOGIN_SYKH 5
#define LOGIN_GYKH 6

@interface TIILoginController () <UITextFieldDelegate,TIIWebViewControllerDelegate>

@property (nonatomic, strong) IBOutlet UIView * container;
@property (nonatomic, strong) IBOutlet UILabel * appVersion;
@property (weak, nonatomic) IBOutlet UIImageView * inputBg;
@property (weak, nonatomic) IBOutlet UIButton * btnLogin;
@property (weak, nonatomic) IBOutlet UITextField * tfUserName;
@property (weak, nonatomic) IBOutlet UITextField * tfPassword;
@property (weak, nonatomic) IBOutlet UITextField * gyPassword;  //工业客户的密码
@property (nonatomic, weak) IBOutlet UIButton *btnSave;         //保存密码
@property (nonatomic, weak) IBOutlet UIButton *btnAutoLogin;    //自动登陆
@property (nonatomic, strong) IBOutlet UIButton *syBtn;         //商业客户
@property (nonatomic, strong) IBOutlet UIButton *gyBtn;         //工业客户
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcInputBgTop;     // 输入框背景top
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *alcInputBgHeight;  // 输入框背景height
@property (nonatomic, strong) IBOutlet UIView *lineView1;       // 用户类型和账号名之间的分割线
@property (nonatomic, strong) IBOutlet UIImageView *ivAccType;  // 用户类型图标
@property (nonatomic, strong) TIIOrgPicker *pickOrg;
@property (nonatomic, strong) IBOutlet UIButton *pickBtn;
@property (nonatomic, strong) NSMutableArray *keyArray;
@property (nonatomic, strong) NSMutableArray *valueArray;
@property (nonatomic) NSInteger process;
@property (nonatomic) BOOL enableClick;

@property (nonatomic) BOOL ableAutoLogin;

@property (nonatomic, strong) NSString *userId;

- (IBAction)login:(id)sender;
- (IBAction)onChange:(UIButton *)btn;
- (IBAction)chooseOne:(UIButton *)btn;


@end

@implementation TIILoginController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.keyArray = [[NSMutableArray alloc] init];
    self.valueArray = [[NSMutableArray alloc] init];
    self.enableClick = NO;
    self.ableAutoLogin = NO;
    
    NSDictionary* infoDict =[[NSBundle mainBundle] infoDictionary];
    self.appVersion.text = [NSString stringWithFormat:@"v%@", [infoDict objectForKey:@"CFBundleShortVersionString"]];
    
    self.inputBg.image = [[UIImage imageNamed:@"TIIInputBox"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 9, 14, 9)];
    //记住密码
    [self.btnSave setImage:[UIImage imageNamed:@"CheckBoxNormal"] forState:UIControlStateNormal];
    [self.btnSave setImage:[UIImage imageNamed:@"CheckBoxChecked"] forState:UIControlStateSelected];
    [self.btnSave addTarget:self action:@selector(btnSaveClicked:) forControlEvents:UIControlEventTouchUpInside];
    //自动登录
    [self.btnAutoLogin setImage:[UIImage imageNamed:@"CheckBoxNormal"] forState:UIControlStateNormal];
    [self.btnAutoLogin setImage:[UIImage imageNamed:@"CheckBoxChecked"] forState:UIControlStateSelected];
    [self.btnAutoLogin addTarget:self action:@selector(btnAutoLoginClicked:) forControlEvents:UIControlEventTouchUpInside];

    [self.btnLogin setBackgroundImage:[[UIImage imageNamed:@"TIIBtnRed"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 9, 14, 9)] forState:UIControlStateNormal];
    [self.btnLogin setBackgroundImage:[[UIImage imageNamed:@"TIIBtnRedPressed"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 9, 14, 9)] forState:UIControlStateHighlighted];
    
    self.indicator.backgroundTouchable = NO;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    self.tfUserName.text = [ud objectForKey:@"syname"];
    self.tfPassword.text = [ud objectForKey:@"sypwd"];
    self.gyPassword.text = [ud objectForKey:@"gypwd"];
    
    if ([ud objectForKey:@"syname"] || [ud objectForKey:@"sypwd"] || [ud objectForKey:@"gyname"] || [ud objectForKey:@"gypwd"]) {
        self.btnSave.selected = YES;
    } else {
        self.btnSave.selected = NO;
    }
    if ([ud objectForKey:@"autologinsy"] || [ud objectForKey:@"autologingy"]) {
        self.btnAutoLogin.selected = YES;
    } else {
        self.btnAutoLogin.selected = NO;
    }
    
    // 不开启工业用户
    if ([IS_OPEN_GY isEqualToString:@"0"]) {
        self.gyBtn.hidden = YES;
        self.syBtn.hidden = YES;
        self.lineView1.hidden = YES;
        self.ivAccType.hidden = YES;
        self.alcInputBgTop.constant += 40;
        self.alcInputBgHeight.constant -= 40;
    }
    
    //选中之前的账户名
    [self DrawGyUser];
    [self onChange:self.syBtn];
    [self loadPickData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    // 不开启工业用户
    if ([IS_OPEN_GY isEqualToString:@"0"]) {
        [self onChange:self.syBtn];
        if ([ud objectForKey:@"autologinsy"]) {
            [self autoSyLogin];
        }
    } else {
        //商业客户自动登录
        if ([ud objectForKey:@"loginType"]) {
            [self onChange:self.syBtn];
            if ([ud objectForKey:@"autologinsy"]) {
                [self autoSyLogin];
            }
            //工业客户自动登录
        } else  {
            [self onChange:self.gyBtn];
            if ([ud objectForKey:@"autologingy"]) {
                if (self.ableAutoLogin == YES) {
                    return;
                }
                [self autoGyLogin];
            }
        }
    }
    return;
}

- (void)autoSyLogin
{
    if ([TIIWebService webServiceHost].length != 0) {
        return;
    }
    
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"自动登陆中..";
    [self.indicator show];
    
    NSString * userName = [self.tfUserName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[TIIWebService getCustLoginInfo:userName] delegate:self];
    conn.tag = GET_LOGININFO;
    [conn start];
}

- (void)autoGyLogin
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *name = [ud objectForKey:@"gyname"];
    int index = [name intValue];
    self.pickOrg.selectedIndex = index;
    NSString * pwd = [ud objectForKey:@"gypwd"];
    self.gyPassword.text = pwd;
    if (self.keyArray.count == 0) {
        self.indicator.text = @"获取账号失败";
        [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
        return;
    }
    
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"自动登陆中..";
    [self.indicator show];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[TIIWebService widePriceCollect:[self.keyArray objectAtIndex:index] password:pwd] delegate:self];
    conn.tag = get_GYVERIFY;
    [conn start];
}

- (void)DrawGyUser
{
    if (self.keyArray.count == 0 || self.valueArray.count == 0) {
        self.keyArray = [NSMutableArray arrayWithObjects:@"35001", @"13001", @"22021", @"31012", @"32001", @"33041", @"34091", @"36001", @"37021", @"41012", @"42001", @"43012", @"44061", @"45009", @"48001", @"52001", @"53010", @"61001", @"62000", @"81012", @"99904", @"99907", @"13500000", nil];
        self.valueArray = [NSMutableArray arrayWithObjects:@"福建中烟", @"河北中烟", @"吉林烟草", @"上海烟草", @"江苏中烟", @"浙江中烟", @"安徽中烟", @"江西中烟",@"山东中烟", @"河南中烟", @"湖北中烟", @"湖南中烟", @"广东中烟", @"广西中烟", @"深圳烟草", @"贵州中烟",@"云南中烟", @"陕西中烟", @"甘肃烟草", @"川渝中烟", @"韩国烟草", @"利是美", @"福建烟草", nil];
    }
    self.pickOrg = [[TIIOrgPicker alloc] initWithTargetButton:self.pickBtn withkeyArray:self.keyArray valueArray:self.valueArray];
    self.enableClick = YES;
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *name = [ud objectForKey:@"gyname"];
    int index = [name intValue];
    if (index >= self.keyArray.count) {
        index = 0;
    }
    self.pickOrg.selectedIndex = index;
}

- (IBAction)onChange:(UIButton *)btn
{
    self.pickBtn.hidden = YES;
    self.tfUserName.hidden = YES;
    self.tfPassword.hidden = YES;
    self.gyPassword.hidden = YES;
    [btn setImage:[UIImage imageNamed:@"Basenormal"] forState:UIControlStateNormal];
    if (btn == self.syBtn) {
        [self.tfUserName resignFirstResponder];
        [self.tfPassword resignFirstResponder];
        [self.gyPassword resignFirstResponder];
        [self.syBtn setImage:[UIImage imageNamed:@"Basechecked"] forState:UIControlStateNormal];
        [self.gyBtn setImage:[UIImage imageNamed:@"Basenormal"] forState:UIControlStateNormal];
        self.tfUserName.hidden = NO;
        self.tfPassword.hidden = NO;
        self.process = LOGIN_SYKH;
        
    } else if (btn == self.gyBtn) {
        [self.tfUserName resignFirstResponder];
        [self.tfPassword resignFirstResponder];
        [self.gyPassword resignFirstResponder];
        [self.gyBtn setImage:[UIImage imageNamed:@"Basechecked"] forState:UIControlStateNormal];
        [self.syBtn setImage:[UIImage imageNamed:@"Basenormal"] forState:UIControlStateNormal];
        self.pickBtn.hidden = NO;
        self.gyPassword.hidden = NO;
        self.process = LOGIN_GYKH;
    }
}

- (IBAction)chooseOne:(UIButton *)btn
{
    [self.pickOrg show];
}

- (void)btnSaveClicked:(UIButton *)btn
{
    btn.selected = !btn.selected;
    if (!self.btnSave.selected) {
        self.btnAutoLogin.selected = NO;
    }
}

- (void)btnAutoLoginClicked:(UIButton *)btn
{
    btn.selected = !btn.selected;
    if (self.btnAutoLogin.selected) {
        self.btnSave.selected = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)loadPickData
{
    if (self.indicator.showing) {
        return;
    }
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[TIIWebService getFactoryiInfoList] delegate:self];
    conn.tag = GET_PICKDATA;
    [conn start];
}

- (void)login:(id)sender
{
    //商业客户登陆
    if (self.process == LOGIN_SYKH) {
        
        if (self.indicator.showing) {
            return;
        }
        
        [self.tfUserName resignFirstResponder];
        [self.tfPassword resignFirstResponder];
        [self.gyPassword resignFirstResponder];
        
        
        NSString * userName = [self.tfUserName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString * pwd = [self.tfPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (userName.length != 11 ) {
            self.indicator.text = @"请输入11位手机号";
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
            return;
        }
        if (pwd.length == 0) {
            self.indicator.text = @"密码不能为空";
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
            return;
        }
        self.indicator.text = @"正在验证..";
        [self.indicator show];
        CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[TIIWebService getCustLoginInfo:userName] delegate:self];
        conn.tag = GET_LOGININFO;
        [conn start];
        
        //工业客户登陆
    } else if (self.process == LOGIN_GYKH) {
        
        [self.tfUserName resignFirstResponder];
        [self.tfPassword resignFirstResponder];
        [self.gyPassword resignFirstResponder];

        NSString * pwd = [self.gyPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (self.indicator.showing) {
            return;
        }
        if (!self.enableClick) {
            self.indicator.text = @"账号不能为空";
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
            return;
        }

        if (pwd.length == 0) {
            self.indicator.text = @"密码不能为空";
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0];
            return;
        }
        self.indicator.text = @"正在验证..";
        [self.indicator show];
        
        CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[TIIWebService widePriceCollect:[self.keyArray objectAtIndex:self.pickOrg.selectedIndex] password:pwd] delegate:self];
        conn.tag = get_GYVERIFY;
        [conn start];
    }
}

- (void)verifyLogin
{
    if (self.indicator.showing) {
        return;
    }
    NSString * pwd = [self.tfPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    self.indicator.text = @"正在验证..";
    [self.indicator show];
    CTURLConnection *conn = [[CTURLConnection alloc] initWithGetMethodUrl:[TIIWebService loginUrl:self.userId password:pwd] delegate:self];
    conn.tag = GET_VERIFYLOGIN;
    [conn start];
}

#pragma mark -
#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.tfUserName) {
        [self.tfPassword becomeFirstResponder];
    } else {
        [self.tfUserName resignFirstResponder];
        [self.tfPassword resignFirstResponder];
        [self.gyPassword resignFirstResponder];
    }
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.tfUserName resignFirstResponder];
    [self.tfPassword resignFirstResponder];
    [self.gyPassword resignFirstResponder];
}

#pragma mark -
#pragma mark CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
    
    if (connection.tag == GET_LOGININFO) {
        self.indicator.text = @"获取地市失败";
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    } else if (connection.tag == GET_VERIFYLOGIN) {
        self.indicator.text = @"登陆失败";
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    } else if (connection.tag == GET_PICKDATA) {
        self.indicator.text = @"加载中..";
        [self.indicator hideWithSate:CTIndicateStateLoading afterDelay:1.0];
        [self DrawGyUser];
    } else if (connection.tag == get_GYVERIFY) {
        self.indicator.text = @"登陆失败";
        [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    }
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    NSError *error;
    if (connection.tag == GET_LOGININFO) {
        TIILoginInfo *info = [[TIILoginInfo alloc] init];
        [info parseData:data];
        self.indicator.showing = NO;
        self.userId = info.r1Person;
        [TIIWebService setWebServiceHost:info.orgUrl];
        [self verifyLogin];
    } else if (connection.tag == GET_VERIFYLOGIN) {
        SystemMsg * msg = [[SystemMsg alloc] init];
        [msg parseData:data complete:^(SystemMsg *msg){
            if ([msg.code isEqualToString:@"1"]) {
                dispatch_queue_t reentrantAvoidanceQueue = dispatch_queue_create("reentrantAvoidanceQueue", DISPATCH_QUEUE_SERIAL);
                dispatch_async(reentrantAvoidanceQueue, ^{
                    BiPerson * person = [[BiPerson alloc] init];
                    [person parseData:data complete:^(NSArray *array){
                        BiPerson * item = [array objectAtIndex:0];
                        item.sessionLogin = YES;
                        item.userName = [self.tfUserName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        item.password = [self.tfPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        [TIIWebService setUser:item];
                        [self.indicator hide];
                        [[TIISqlLite shareManager] createMsgTable];
                        [self dismissViewControllerAnimated:YES completion:nil];
                        //保存账号密码 和当前登陆的哪个客户
                        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                        [ud setObject:@"SYHK" forKey:@"loginType"];
                        if (self.btnSave.selected) {
                            [ud setObject:item.userName forKey:@"syname"];
                            [ud setObject:item.password forKey:@"sypwd"];
                        } else {
                            [ud removeObjectForKey:@"syname"];
                            [ud removeObjectForKey:@"sypwd"];
                            [ud removeObjectForKey:@"gyname"];
                            [ud removeObjectForKey:@"gypwd"];
                        }
                        if (self.btnAutoLogin.selected) {
                            [ud setObject:@"autologinsy" forKey:@"autologinsy"];
                        } else {
                            [ud removeObjectForKey:@"autologinsy"];
                        }
                        [ud synchronize];
                    }];
                });
                dispatch_sync(reentrantAvoidanceQueue, ^{ });
            } else {
                self.indicator.text = msg.msg;
                [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
            }
        }];
    } else if (connection.tag == GET_PICKDATA) {
        
        [self.keyArray removeAllObjects];
        [self.valueArray removeAllObjects];
        NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
        for (int i = 0; i < array.count; i ++) {
           NSString *code = [[array objectAtIndex:i] objectForKey:@"USER_CODE"];
           NSString *name = [[array objectAtIndex:i] objectForKey:@"USER_NAME"];
            [self.keyArray addObject:code];
            [self.valueArray addObject:name];
        }
        [self DrawGyUser];
        
    } else if (connection.tag == get_GYVERIFY) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
        NSString *gyname = [dict objectForKey:@"USER_CODE"];
        if (gyname) {
            [self.indicator hide];
            
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud removeObjectForKey:@"loginType"];
            if (self.btnSave.selected) {
                [ud setInteger:self.pickOrg.selectedIndex forKey:@"gyname"];
                [ud setObject:self.gyPassword.text forKey:@"gypwd"];
            } else {
                [ud removeObjectForKey:@"syname"];
                [ud removeObjectForKey:@"sypwd"];
                [ud removeObjectForKey:@"gyname"];
                [ud removeObjectForKey:@"gypwd"];
            }
            if (self.btnAutoLogin.selected) {
                [ud setObject:@"autologingy" forKey:@"autologingy"];
            } else {
                [ud removeObjectForKey:@"autologingy"];
            }

            [ud synchronize];


            TIIWebViewController *controller = [[TIIWebViewController alloc] init];
            controller.companyCode = [dict objectForKey:@"USER_CODE"];
            controller.delegate = self;
            [self presentViewController:controller animated:YES completion:nil];
            
        } else {
            self.indicator.text = @"登陆失败";
            [self.indicator autoHide:CTIndicateStateWarning afterDelay:1.0f];
        }
    }
}

#pragma mark - TIIWebViewDelegate
- (void)recvDiss:(TIIWebViewController *)controller Bool:(BOOL)able
{
    self.ableAutoLogin = able;
}

@end
