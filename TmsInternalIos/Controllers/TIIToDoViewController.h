//
//  TIIToDoViewController.h
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-21.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "CTViewController.h"

@class TIIToDoViewController;

@protocol TIIToDoViewControllerDelegate <NSObject>

@optional
- (void)tiiToDoViewControllerDelegate:(TIIToDoViewController *)controller pushController:(UIViewController *)tagController;
- (void)tiiToDoViewControllerDelegate:(TIIToDoViewController *)controller presentController:(UIViewController *)tagController;

@end

@interface TIIToDoViewController : CTViewController

@property (nonatomic, weak) id<TIIToDoViewControllerDelegate> delegate;

@end
