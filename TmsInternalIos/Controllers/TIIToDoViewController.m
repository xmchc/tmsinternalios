//
//  TIIToDoViewController.m
//  TmsInternalIos
//
//  Created by 张斌 on 14-7-21.
//  Copyright (c) 2014年 hsit. All rights reserved.
//

#import "TIIToDoViewController.h"
#import "CTTableView.h"
#import "TIIWebService.h"
#import "TIICommonMsg.h"
#import "CTSoapConnection.h"
#import "TIISqlLite.h"
#import "TIIMsgDetailController.h"
#import "TIIWebViewController.h"
#import "TIILrpMenu.h"
#import "CTURLConnection.h"

#define URL_TAG_LOADDATA 0
#define URL_TAG_UPDATEREADSTATUS 100
#define URL_TAG_GETISSUESTATUS 1000

@interface TIIToDoViewController () <CTTableViewDelegate, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, CTSoapConnectionDelegate>

@property (nonatomic, strong) CTTableView *tv;
@property (nonatomic, strong) NSMutableArray *array;

- (void)loadData;
- (void)updateReadStatus:(NSString *)msgId withTag:(NSInteger)tag;
- (void)drawView;
- (void)sectionExpandClicked:(UIControl *)control;
- (void)btnHandleClicked:(UIButton *)button;

@end

@implementation TIIToDoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.array = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.tv == nil) {
        self.tv = [[CTTableView alloc] initWithFrame:self.view.bounds];
        self.tv.backgroundColor = [UIColor clearColor];
        self.tv.pullUpViewEnabled = NO;
        self.tv.ctTableViewDelegate = self;
        self.tv.dataSource = self;
        self.tv.delegate = self;
        [self.view addSubview:self.tv];
        
        [self loadData];
    } else {
        if ([TIIWebService unreadCount].unreadToDoCnt > 0) {
            [self loadData];
        }
    }
    
    [self checkIssueStatus];
    return;
}

#pragma mark -
#pragma mark click event
- (void)sectionExpandClicked:(UIControl *)control
{
    TIICommonMsg *curToDo = [self.array objectAtIndex:control.tag];
    curToDo.expand = !curToDo.expand;
    NSIndexSet *set = [[NSIndexSet alloc] initWithIndex:control.tag];
    [self.tv reloadSections:set withRowAnimation:UITableViewRowAnimationFade];
    return;
}

- (void)btnHandleClicked:(UIButton *)button
{
    TIICommonMsg *curToDo = [self.array objectAtIndex:button.tag];
    NSString *issueId = [curToDo.MSG_URL_PAD substringFromIndex:5];
    TIIWebViewController *controller = [[TIIWebViewController alloc] init];
    TIILrpMenu *menu = [[TIILrpMenu alloc] init];
    menu.appUrl = [TIIWebService getEventUrl];
    controller.menu = menu;
    controller.issueId = issueId;
    [self.delegate tiiToDoViewControllerDelegate:self presentController:controller];
    
    //标志已处理
    [self updateReadStatus:curToDo.msgId withTag:URL_TAG_UPDATEREADSTATUS + button.tag];

}

#pragma mark -
#pragma mark url request
- (void)loadData
{
    if (self.indicator.showing) {
        return;
    }
    self.indicator.text = @"正在加载..";
    [self.indicator show];
    
    NSString *params = [TIIWebService getMsgByTypeParams:MSG_TYPE_TODO];
    
    CTSoapConnection * conn = [[CTSoapConnection alloc] initWithFunctionName:WSDL_FUNCNAME_GETMSGBYTYPE params:params delegate:self];
    conn.tag = URL_TAG_LOADDATA;
    [conn start];
    return;
}

- (void)updateReadStatus:(NSString *)msgId withTag:(NSInteger)tag
{
    NSString *params = [TIIWebService updateReadStatusParams:msgId];
    
    CTSoapConnection * conn = [[CTSoapConnection alloc] initWithFunctionName:WSDL_FUNCNAME_UPDATEREADSTATUS params:params delegate:self];
    conn.tag = tag;
    [conn start];
    return;
}

- (void)checkIssueStatus
{
    NSArray *allMsgArray = [[TIISqlLite shareManager] getMsgByType:MSG_TYPE_TODO];
    for (TIICommonMsg *msg in allMsgArray) {
        NSString *issueId = [msg.MSG_URL_PAD substringFromIndex:5];
        CTURLConnection * conn = [[CTURLConnection alloc] initWithGetMethodUrl:[TIIWebService getIssueStatusUrl:issueId] delegate:self];
        conn.tag = [issueId intValue];
        [conn start];
    }
    return;
}

- (void)drawView
{
    NSArray *allMsgArray = [[TIISqlLite shareManager] getMsgByType:MSG_TYPE_TODO];
    self.array  = [NSMutableArray arrayWithArray:allMsgArray];
    
    [self.tv reloadData];
    return;
}

#pragma mark -
#pragma mark UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.tv ctTableViewDidScroll];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self.tv ctTableViewDidEndDragging];
}

#pragma mark -
#pragma mark CTTableView DataSource & Delegate
- (BOOL)ctTableViewDidPullDownToRefresh:(CTTableView *)ctTableView
{
    if (self.indicator.showing) {
        return NO;
    }
    [self loadData];
    return YES;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger height = 0;
    if (indexPath.row == 0) {
        height = 45;
    } else {
        height = 90;
    }
    return height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    TIICommonMsg *curToDo = [self.array objectAtIndex:section];
    if (curToDo.expand) {
        return 2;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"CELL";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    TIICommonMsg *curItem = [self.array objectAtIndex:indexPath.section];
    if (indexPath.row == 0) {
        // header
        NSArray *header = [[NSBundle mainBundle] loadNibNamed:@"TIISystemPublicHeaderCell" owner:nil options:nil];
        UIView *headerView = [header objectAtIndex:0];
        CGRect headerViewFrame = headerView.frame;
        headerViewFrame.size.width = ScreenWidth;
        headerView.frame = headerViewFrame;
        
        UILabel  *titleLabel = (UILabel *)[headerView viewWithTag:10];
        titleLabel.text = curItem.msgTitle;
        
        UIControl *backControl = (UIControl *)[headerView viewWithTag:1];
        backControl.tag = indexPath.section;
        [backControl addTarget:self action:@selector(sectionExpandClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView *ivLine = [[UIImageView alloc] init];
        ivLine.image = [[UIImage imageNamed:@"Xuxian"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        ivLine.frame = CGRectMake(15, 44, ScreenWidth - 30, 1);
        [headerView addSubview:ivLine];
        [cell addSubview:headerView];
    } else {
        NSArray *elements = [[NSBundle mainBundle] loadNibNamed:@"TIIToDoCell" owner:nil options:nil];
        UIView *view = [elements objectAtIndex:0];
        CGRect viewFrame = view.frame;
        viewFrame.size.width = ScreenWidth;
        view.frame = viewFrame;
        [cell addSubview:view];
        
        UILabel *labDate = (UILabel *)[cell viewWithTag:1];
        UILabel *labContent = (UILabel *)[cell viewWithTag:2];
        UIButton *btnHandle = (UIButton *)[cell viewWithTag:3];
        
        labDate.text = curItem.msgDate;
        labContent.text = curItem.msgContent;
        btnHandle.tag = indexPath.section;
        UIImage *cancelCare = [[UIImage imageNamed:@"Handle"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 22, 3, 3)];
        UIImage *cancelCareHightLight = [[UIImage imageNamed:@"HandleHighLight"] resizableImageWithCapInsets:UIEdgeInsetsMake(3, 22, 3, 3)];
        [btnHandle setBackgroundImage:cancelCare forState:UIControlStateNormal];
        [btnHandle setBackgroundImage:cancelCareHightLight forState:UIControlStateHighlighted];
        [btnHandle addTarget:self action:@selector(btnHandleClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self.delegate respondsToSelector:@selector(tiiToDoViewControllerDelegate:pushController:)]) {
        TIICommonMsg *curPublic = [self.array objectAtIndex:indexPath.section];
        TIIMsgDetailController *tagController = [[TIIMsgDetailController alloc] init];
        tagController.curSysPublic = curPublic;
        tagController.title = @"待办详情";
        [self.delegate tiiToDoViewControllerDelegate:self pushController:tagController];
    }
}

#pragma mark -
#pragma mark CTSoapConnection DataSource & Delegate
// 请求结束执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFinishLoading:(NSData *)data
{
    if (connection.tag == URL_TAG_LOADDATA) {
//        NSLog(@"ToDo:%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        TIICommonMsg *toDo = [[TIICommonMsg alloc] init];
        [toDo parseData:data complete:^(NSArray *array) {
            [self.indicator hide];
            [self.array removeAllObjects];
            // 存数据库
            [[TIISqlLite shareManager] addNewMsg:array];
            [self drawView];
            
            // 更改已阅状态
            for (int i = 0; i < array.count; i++) {
                TIICommonMsg *unreadMsg = [array objectAtIndex:i];
                [self updateReadStatus:unreadMsg.msgId withTag:URL_TAG_UPDATEREADSTATUS + i];
            }
            
            // 更改未读数量
            TIIUnreadCount *unreadCnt = [TIIWebService unreadCount];
            unreadCnt.unreadToDoCnt = 0;
            [TIIWebService setUnreadCount:unreadCnt];
            // 发送消息数量通知
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFCATION_MESSAGE_COUNT object:nil];
        }];
    } else if (connection.tag >= URL_TAG_UPDATEREADSTATUS) {
//        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//        NSLog(@"tag=%d result=%@", connection.tag, result);
        return;
    }
    return;
}

// 请求失败时执行的方法
- (void)soapConnection:(CTSoapConnection *)connection didFailWithError:(NSError *)error
{
    self.indicator.text = @"加载失败";
    [self.indicator hideWithSate:CTIndicateStateWarning afterDelay:1.0];
    if (connection.tag == URL_TAG_LOADDATA) {
        [self drawView];
        return;
    }
    [self.tv reloadData];
    return;
}

#pragma mark - CTURLConnection Delegate
- (void)connection:(CTURLConnection *)connection didFailWithError:(NSError *)error
{
}

- (void)connection:(CTURLConnection *)connection didFinishLoading:(NSData *)data
{
    NSString *resultStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSInteger result = [resultStr intValue];
    NSString *issueId = [NSString stringWithFormat:@"%d", connection.tag];
    if (result == 3 || result == 6) {
        // 处理完成
        [[TIISqlLite shareManager] deleteMsgByIssueId:issueId];
        [self drawView];
    }
    return;
}

@end
