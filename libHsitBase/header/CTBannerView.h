//
//  CTBannerView.h
//  CmAppCustIos
//
//  Created by 陈 宏超 on 14-1-25.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTBannerView;

@protocol CTBannerViewDelegate <NSObject>

- (void)ctBannerViewDidScroll:(CTBannerView *)view;

@end


@interface CTBannerView : UIView

@property (nonatomic) BOOL autoScroll;
@property (nonatomic, strong) NSArray * viewArray;
@property (nonatomic, weak) id<CTBannerViewDelegate> delegate;
@property (nonatomic) int mMaxPage;

- (int)currentPage;
- (void)setCurrentPage:(NSUInteger)page;
- (void)invalidate;
- (void)setIndicateColor:(UIColor *)normalColor highlightedColor:(UIColor *)highlightedColor;

@end
