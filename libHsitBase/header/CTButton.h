//
//  CTButton.h
//  CmAppConsumerIos
//
//  Created by 陈 宏超 on 14-4-9.
//  Copyright (c) 2014年 ICSSHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTButton : UIButton

@property (nonatomic) int index;
@property (nonatomic, strong) NSString * specialId;

@end
